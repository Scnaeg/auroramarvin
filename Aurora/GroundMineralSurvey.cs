﻿namespace AuroraMarvin.Aurora
{
    public enum GroundMineralSurvey
    {
#pragma warning disable SA1602 // EnumerationItemsMustBeDocumented
        Completed,
        Minimal,
        Low,
        Good,
        High,
        Excellent,
#pragma warning restore SA1602 // EnumerationItemsMustBeDocumented
    }
}
