﻿namespace AuroraMarvin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    public partial class AuroraMarvinView : Form, IAuroraMarvinView
    {
        private const string AURORAVERSION = "2.2.0";
        private const string FORUMURL = "http://aurora2.pentarch.org/index.php?topic=12233.msg145907";
        private readonly Version marvinVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        private IAuroraMarvinController controller;
        private bool showTechTree;
        private Dictionary<string, bool> showMsgItem;
        private Dictionary<string, string[]> overviewMsgs;
        private int startYear;

        public AuroraMarvinView()
        {
            this.InitializeComponent();
#if DEBUG
            this.Text += " [DEBUG]";
#endif
            this.Text += $" (v{this.marvinVersion} for Aurora 4x C# {AURORAVERSION})";
        }

        public void SetController(IAuroraMarvinController cont)
        {
            this.controller = cont;

            DataTable menuItemState = this.controller.GetMenuItemState();

            string[] menuItems =
            {
                "ShowAetherRiftGrowthRateReductionToolStripMenuItem",
                "ShowDamagedShipsToolStripMenuItem",
                "ShowLowCrewMoraleToolStripMenuItem",
                "ShowFreeConstructionFactoriesToolStripMenuItem",
                "ShowFreeOrdnanceFactoriesToolStripMenuItem",
                "ShowFreeFighterFactoriesToolStripMenuItem",
                "ShowObsoleteShipsToolStripMenuItem",
                "ShowUnusedTerraformersToolStripMenuItem",
                "ShowUnusedMinesToolStripMenuItem",
                "ShowShipsWithArmorDamageToolStripMenuItem",
                "ShowWrecksToolStripMenuItem",
                "ShowObsoleteTooledShipyardsToolStripMenuItem",
                "ShowMissingSectorCommandersToolStripMenuItem",
                "ShowShipWithoutMspToolStripMenuItem",
                "ShowShipWithLowMspToolStripMenuItem",
                "ShowMissingAdminCommandersToolStripMenuItem",
                "ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem",
                "ShowTaxedCivMinesToolStripMenuItem",
                "ShowLowPopulationEfficienyToolStripMenuItem",
                "ShowSelfSustainingColonistDestinationToolStripMenuItem",
                "ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem",
                "ShowLifePodsToolStripMenuItem",
                "ShowPopulationsWithoutGovernorToolStripMenuItem",
                "ShowOpenFireFCToolStripMenuItem",
                "ShowResearchFieldMismatchToolStripMenuItem",
                "ShowResearchWithoutResearchFacilityToolStripMenuItem",
                "ShowSystemsWithUnsurveyedBodiesToolStripMenuItem",
                "ShowIdleSoriumHarvestersToolStripMenuItem",
                "ShowIdleOrbitalMinersToolStripMenuItem",
                "ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem",
                "ShowIdleGeosurveyFormationsToolStripMenuItem",
                "ShowDormantAncientConstructsToolStripMenuItem",
                "ShowNotActiveAncientConstructsToolStripMenuItem",
                "ShowHostileShipContactsToolStripMenuItem",
                "ShowHostileGroundForceContactsToolStripMenuItem",
                "ShowIdleShipyardsToolStripMenuItem",
                "ShowIdleGroundForceConstructionComplexToolStripMenuItem",
            };

            this.showMsgItem = new Dictionary<string, bool>();
            this.overviewMsgs = new Dictionary<string, string[]>();

            if (menuItemState.Rows.Count != 0)
            {
                DataRow[] techTreeMenuItem = menuItemState.Select("Name = 'ShowTechTreeToolStripMenuItem'");
                if (techTreeMenuItem.Length != 0)
                {
                    this.showTechTree = Convert.ToBoolean(Convert.ToInt16(techTreeMenuItem[0]["state"].ToString()));
                    this.ShowTechTreeToolStripMenuItem.Checked = this.showTechTree;
                }
                else
                {
                    this.showTechTree = true;
                }

                foreach (string item in menuItems)
                {
                    this.showMsgItem[item] = true;
                    DataRow[] state = menuItemState.Select("Name = '" + item + "'");
                    if (state.Length != 0)
                    {
                        ToolStripMenuItem menuItem = this.viewToolStripMenuItem.DropDownItems[item] as ToolStripMenuItem;
                        bool c = Convert.ToBoolean(Convert.ToInt16(state[0]["state"].ToString()));
                        menuItem.Checked = c;
                        this.showMsgItem[item] = c;
                    }
                }
            }
            else
            {
                this.showTechTree = this.ShowTechTreeToolStripMenuItem.Checked;
                foreach (string item in menuItems)
                {
                    this.showMsgItem[item] = true;
                }
            }
        }

        public void SetGames(DataTable games)
        {
            this.gameSelector.DisplayMember = "GameName";
            this.gameSelector.ValueMember = "GameID";
            this.gameSelector.DataSource = games;
            DataRow[] lastGame = games.Select("LastViewed = 1");
            this.gameSelector.SelectedValue = lastGame[0]["GameID"];
        }

        public void SetRaces(DataTable races)
        {
            this.raceSelector.DisplayMember = "RaceName";
            this.raceSelector.ValueMember = "RaceID";
            this.raceSelector.DataSource = races;
        }

        public void SetPopulations(DataTable populations)
        {
            this.populationViewer.DataSource = populations;
            this.PopulationComboBox1.DisplayMember = "PopName";
            this.PopulationComboBox1.ValueMember = "PopulationID";
            this.PopulationComboBox1.DataSource = populations;
        }

        public void SetShips(DataTable ships)
        {
            this.shipViewer.DataSource = ships;
        }

        public void AddOverviewMessage(string key, List<string> msg)
        {
            this.overviewMsgs.Add(key, msg.ToArray());
        }

        public void SetHullDescriptions(DataTable hullDescriptions)
        {
            this.hullSelector.DataSource = hullDescriptions;
        }

        public void SetDesignsForHull(DataTable shipDesigns)
        {
            this.shipDesignSelector.DataSource = null;
            this.shipDesignSelector.Items.Clear();
            this.shipDesignSelector.DisplayMember = "Name";
            this.shipDesignSelector.ValueMember = "Name";
            this.shipDesignSelector.DataSource = shipDesigns;
        }

        public void SetShipDesign(DataTable design)
        {
            this.shipDisplay.Text = design.Rows[0]["Design"].ToString();
        }

        public void SetNotes(DataTable notes)
        {
            const string SZ_RTF_TAG = "{\\rtf";
            string note = notes.Rows[0]["Note"].ToString();
            if (note.StartsWith(SZ_RTF_TAG))
            {
                MemoryStream stream = new MemoryStream(ASCIIEncoding.Default.GetBytes(note));
                this.notesBox.LoadFile(stream, RichTextBoxStreamType.RichText);
            }
            else
            {
                this.notesBox.Text = note;
            }
        }

        public void SetGameNotes(DataTable notes)
        {
            if (notes.Rows.Count == 0)
            {
                this.gameNotesBox.Clear();
            }
            else
            {
                string note = notes.Rows[0]["Note"].ToString();
                MemoryStream stream = new MemoryStream(ASCIIEncoding.Default.GetBytes(note));
                this.gameNotesBox.LoadFile(stream, RichTextBoxStreamType.RichText);
            }
        }

        public void SetGameTime(DateTime gameTime)
        {
            this.GameTimeLabel.Text = gameTime.ToString();
        }

        public void SetResources(DataTable ressources)
        {
            DataTable re = this.AdjustResourceDataTime(ressources);

            this.chart1.DataSource = re;
            this.chart1.DataBind();
            this.chart1.Update();
            this.chart2.DataSource = re;
            this.chart2.DataBind();
            this.chart2.Update();
            this.chart3.DataSource = re;
            this.chart3.DataBind();
            this.chart3.Update();
            this.chart4.DataSource = re;
            this.chart4.DataBind();
            this.chart4.Update();

            Dictionary<string, double> minerals = new Dictionary<string, double>
            {
                { "Duranium", 0 },
                { "Neutronium", 0 },
                { "Corbomite", 0 },
                { "Tritanium", 0 },
                { "Boronide", 0 },
                { "Mercassium", 0 },
                { "Vendarite", 0 },
                { "Sorium", 0 },
                { "Uridium", 0 },
                { "Corundium", 0 },
                { "Gallicite", 0 },
                { "Total", 0 },
            };

            Dictionary<string, double> lastMinerals = new Dictionary<string, double>(minerals);

            this.mineralChangesGridView.Rows.Clear();
            this.mineralChangesGridView.ColumnCount = 13;
            this.mineralChangesGridView.Columns[0].HeaderText = "GameTime";

            for (int i = 0; i < minerals.Count; i++)
            {
                this.mineralChangesGridView.Columns[i + 1].HeaderText = minerals.Keys.ElementAt(i);
            }

            foreach (DataRow r in ressources.Rows)
            {
                string gameTime = r["GameTime"].ToString();
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(this.mineralChangesGridView);
                row.Cells[0].Value = gameTime;

                double total = 0;
                int i = 1;

                foreach (var m in lastMinerals)
                {
                    double mt;
                    if (m.Key == "Total")
                    {
                        mt = total;
                    }
                    else
                    {
                        mt = double.Parse(r["SUM(" + m.Key + ")"].ToString());
                    }

                    minerals[m.Key] = mt;
                    total += mt;
                    row.Cells[i].Value = mt;
                    if (mt > lastMinerals[m.Key])
                    {
                        row.Cells[i].Style.BackColor = Color.Green;
                    }

                    if (mt < lastMinerals[m.Key])
                    {
                        row.Cells[i].Style.BackColor = Color.Red;
                    }

                    i++;
                }

                row.Cells[1].Value = total;

                lastMinerals = new Dictionary<string, double>(minerals);

                this.mineralChangesGridView.Rows.Add(row);
            }

            this.mineralChangesGridView.Sort(this.mineralChangesGridView.Columns[0], System.ComponentModel.ListSortDirection.Descending);
        }

        public void SetWealthPoints(DataTable dataTable)
        {
            DataTable re = this.AdjustResourceDataTime(dataTable);

            this.chart5.DataSource = re;
            this.chart5.DataBind();
            this.chart5.Update();
        }

        public void SetEventColours(DataTable events)
        {
            this.eventColourView.Rows.Clear();
            this.eventColourView.ColumnCount = 1;
            this.eventColourView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            foreach (DataRow row in events.Rows)
            {
                DataGridViewRow r = new DataGridViewRow();
                r.CreateCells(this.eventColourView);
                r.Cells[0].Value = row["Description"];
                r.Cells[0].Style.BackColor = Color.FromArgb(int.Parse(row["AlertColour"].ToString()));
                r.Cells[0].Style.ForeColor = Color.FromArgb(int.Parse(row["TextColour"].ToString()));
                this.eventColourView.Rows.Add(r);
            }
        }

        public void ClearOverviewMessages()
        {
            this.overviewMsgs.Clear();
        }

        public void RefreshOverviewMessage()
        {
            this.overviewList.Items.Clear();
            foreach (string key in this.overviewMsgs.Keys)
            {
                if (this.showMsgItem[key])
                {
                    this.overviewList.Items.AddRange(this.overviewMsgs[key]);
                }
            }
        }

        public void SetFuelInTankers(DataTable fuel)
        {
            string litre = fuel.Rows[0]["FuelInTankers"].ToString();
            this.fuelTankerLabel.Text = "Fuel in tankers: " + litre + " litre";
        }

        public void SetGameStartTime(int startyear)
        {
            this.startYear = startyear;
            if (this.startYear < 100)
            {
                int newStartYear = this.startYear + (100 - this.startYear);

                DateTime startDateTime = new DateTime(newStartYear, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Local);
                this.chart1.ChartAreas[0].AxisX.Minimum = startDateTime.ToOADate();
                this.chart2.ChartAreas[0].AxisX.Minimum = startDateTime.ToOADate();
                this.chart3.ChartAreas[0].AxisX.Minimum = startDateTime.ToOADate();
                this.chart4.ChartAreas[0].AxisX.Minimum = startDateTime.ToOADate();
                this.chart5.ChartAreas[0].AxisX.Minimum = startDateTime.ToOADate();
                this.chart6.ChartAreas[0].AxisX.Minimum = startDateTime.ToOADate();
                this.DisplayStartingYearWorkaroundWarning(true);
            }
            else
            {
                this.DisplayStartingYearWorkaroundWarning(false);
            }
        }

        private void GameSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.gameSelector.DisplayMember = "GameName";
            this.gameSelector.ValueMember = "GameID";
            this.controller.SetGame(int.Parse(this.gameSelector.SelectedValue.ToString()));
            this.overviewList.Items.Clear();
            this.controller.GetRaces();
        }

        private void RaceSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.raceSelector.DisplayMember = "RaceName";
            this.raceSelector.ValueMember = "RaceID";
            this.controller.SetRace(int.Parse(this.raceSelector.SelectedValue.ToString()));
            this.overviewList.Items.Clear();
            this.controller.GetData();
        }

        private void FileButton_Click(object sender, EventArgs e)
        {
            DialogResult result = this.openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                string dbfile = this.openFileDialog1.FileName;
                this.overviewList.Items.Clear();
                this.controller.SetDatabaseFile(dbfile);
                this.controller.GetGames();

                this.RenderTechTree();

                this.tabControl1.Visible = true;

                Cursor.Current = Cursors.Default;

                this.fileSystemWatcher1.Path = Path.GetDirectoryName(dbfile);
                this.fileSystemWatcher1.Changed += this.FileSystemWatcher1_Changed;
                this.fileSystemWatcher1.NotifyFilter = NotifyFilters.LastWrite;
                this.fileSystemWatcher1.Filter = "AuroraDB.db";
            }
        }

        private void RenderTechTree()
        {
            if (this.showTechTree)
            {
                if (this.gameSelector.Items.Count != 0)
                {
                    Microsoft.Msagl.GraphViewerGdi.GViewer viewer = new Microsoft.Msagl.GraphViewerGdi.GViewer();
                    Microsoft.Msagl.Drawing.Graph graph = this.controller.GetTechTree();
                    viewer.CurrentLayoutMethod = Microsoft.Msagl.GraphViewerGdi.LayoutMethod.MDS;
                    viewer.Graph = graph;
                    viewer.Dock = System.Windows.Forms.DockStyle.Fill;
                    this.tabPage10.SuspendLayout();
                    this.tabPage10.Controls.Add(viewer);
                    this.tabPage10.ResumeLayout();

                    if (!this.tabControl1.TabPages.Contains(this.tabPage10))
                    {
                        this.tabControl1.TabPages.Add(this.tabPage10);
                    }
                }
            }
            else
            {
                this.tabControl1.TabPages.Remove(this.tabPage10);
            }
        }

        private void HullSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.controller.GetDesignsForHull(int.Parse(this.hullSelector.SelectedValue.ToString()));
        }

        private void ShipDesignSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.shipDesignSelector.SelectedValue != null)
            {
                this.controller.GetShipDesign(this.shipDesignSelector.SelectedValue.ToString());
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            this.controller.SaveShipDesign(this.hullSelector.SelectedValue.ToString(), this.shipName.Text, this.shipDisplay.Text);
            this.shipDisplay.Text = string.Empty;
            this.controller.GetDesignsForHull(int.Parse(this.hullSelector.SelectedValue.ToString()));
        }

        private void NotesSaveButton_Click(object sender, EventArgs e)
        {
            this.controller.UpdateNotes(this.notesBox.Rtf);
        }

        private void GameNotesSaveButton_Click(object sender, EventArgs e)
        {
            this.controller.UpdateGameNotes(this.gameNotesBox.Rtf);
        }

        private void FileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                this.fileSystemWatcher1.EnableRaisingEvents = false;
                if (this.controller.CheckForChangedDatabase())
                {
                    Console.WriteLine("Database {0} changed. Reloading data.", e.Name);
                    Cursor.Current = Cursors.WaitCursor;
                    this.overviewList.Items.Clear();
                    this.controller.SavePopulationResources();
                    this.controller.SaveWealthPoints();
                    this.controller.GetData();
                    Cursor.Current = Cursors.Default;
                }
            }
            finally
            {
                this.fileSystemWatcher1.EnableRaisingEvents = true;
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ShowTechTreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            this.controller.SetMenuItemState(this.ShowTechTreeToolStripMenuItem.Name, this.ShowTechTreeToolStripMenuItem.Checked);
            this.showTechTree = this.ShowTechTreeToolStripMenuItem.Checked;
            this.RenderTechTree();
            Cursor.Current = Cursors.Default;
        }

        private void ShowAetherRiftGrowthRateReductionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.Name, this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.Checked);
            this.showMsgItem["ShowAetherRiftGrowthRateReductionToolStripMenuItem"] = this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowDamagedShipsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowDamagedShipsToolStripMenuItem.Name, this.ShowDamagedShipsToolStripMenuItem.Checked);
            this.showMsgItem["ShowDamagedShipsToolStripMenuItem"] = this.ShowDamagedShipsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowLowCrewMoraleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowLowCrewMoraleToolStripMenuItem.Name, this.ShowLowCrewMoraleToolStripMenuItem.Checked);
            this.showMsgItem["ShowLowCrewMoraleToolStripMenuItem"] = this.ShowLowCrewMoraleToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowFreeConstructionFactoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowFreeConstructionFactoriesToolStripMenuItem.Name, this.ShowFreeConstructionFactoriesToolStripMenuItem.Checked);
            this.showMsgItem["ShowFreeConstructionFactoriesToolStripMenuItem"] = this.ShowFreeConstructionFactoriesToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowFreeOrdnanceFactoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowFreeOrdnanceFactoriesToolStripMenuItem.Name, this.ShowFreeOrdnanceFactoriesToolStripMenuItem.Checked);
            this.showMsgItem["ShowFreeOrdnanceFactoriesToolStripMenuItem"] = this.ShowFreeOrdnanceFactoriesToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowFreeFighterFactoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowFreeFighterFactoriesToolStripMenuItem.Name, this.ShowFreeFighterFactoriesToolStripMenuItem.Checked);
            this.showMsgItem["ShowFreeFighterFactoriesToolStripMenuItem"] = this.ShowFreeFighterFactoriesToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowObsoleteShipsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowObsoleteShipsToolStripMenuItem.Name, this.ShowObsoleteShipsToolStripMenuItem.Checked);
            this.showMsgItem["ShowObsoleteShipsToolStripMenuItem"] = this.ShowObsoleteShipsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowUnusedTerraformersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowUnusedTerraformersToolStripMenuItem.Name, this.ShowUnusedTerraformersToolStripMenuItem.Checked);
            this.showMsgItem["ShowUnusedTerraformersToolStripMenuItem"] = this.ShowUnusedTerraformersToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowUnusedMinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowUnusedMinesToolStripMenuItem.Name, this.ShowUnusedMinesToolStripMenuItem.Checked);
            this.showMsgItem["ShowUnusedMinesToolStripMenuItem"] = this.ShowUnusedMinesToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowShipsWithArmorDamageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowShipsWithArmorDamageToolStripMenuItem.Name, this.ShowShipsWithArmorDamageToolStripMenuItem.Checked);
            this.showMsgItem["ShowShipsWithArmorDamageToolStripMenuItem"] = this.ShowShipsWithArmorDamageToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowWrecksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowWrecksToolStripMenuItem.Name, this.ShowWrecksToolStripMenuItem.Checked);
            this.showMsgItem["ShowWrecksToolStripMenuItem"] = this.ShowWrecksToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowObsoleteTooledShipyardsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowObsoleteTooledShipyardsToolStripMenuItem.Name, this.ShowObsoleteTooledShipyardsToolStripMenuItem.Checked);
            this.showMsgItem["ShowObsoleteTooledShipyardsToolStripMenuItem"] = this.ShowObsoleteTooledShipyardsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowMissingSectorCommandersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowMissingSectorCommandersToolStripMenuItem.Name, this.ShowMissingSectorCommandersToolStripMenuItem.Checked);
            this.showMsgItem["ShowMissingSectorCommandersToolStripMenuItem"] = this.ShowMissingSectorCommandersToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowShipWithoutMspToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowShipWithoutMspToolStripMenuItem.Name, this.ShowShipWithoutMspToolStripMenuItem.Checked);
            this.showMsgItem["ShowShipWithoutMspToolStripMenuItem"] = this.ShowShipWithoutMspToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowShipWithLowMspToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowShipWithLowMspToolStripMenuItem.Name, this.ShowShipWithLowMspToolStripMenuItem.Checked);
            this.showMsgItem["ShowShipWithLowMspToolStripMenuItem"] = this.ShowShipWithLowMspToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowMissingAdminCommandersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowMissingAdminCommandersToolStripMenuItem.Name, this.ShowMissingAdminCommandersToolStripMenuItem.Checked);
            this.showMsgItem["ShowMissingAdminCommandersToolStripMenuItem"] = this.ShowMissingAdminCommandersToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.Name, this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.Checked);
            this.showMsgItem["ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem"] = this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowTaxedCivMinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowTaxedCivMinesToolStripMenuItem.Name, this.ShowTaxedCivMinesToolStripMenuItem.Checked);
            this.showMsgItem["ShowTaxedCivMinesToolStripMenuItem"] = this.ShowTaxedCivMinesToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowLowPopulationEfficienyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowLowPopulationEfficienyToolStripMenuItem.Name, this.ShowLowPopulationEfficienyToolStripMenuItem.Checked);
            this.showMsgItem["ShowLowPopulationEfficienyToolStripMenuItem"] = this.ShowLowPopulationEfficienyToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowSelfSustainingColonistDestinationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowSelfSustainingColonistDestinationToolStripMenuItem.Name, this.ShowSelfSustainingColonistDestinationToolStripMenuItem.Checked);
            this.showMsgItem["ShowSelfSustainingColonistDestinationToolStripMenuItem"] = this.ShowSelfSustainingColonistDestinationToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.Name, this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.Checked);
            this.showMsgItem["ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem"] = this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowLifePodsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowLifePodsToolStripMenuItem.Name, this.ShowLifePodsToolStripMenuItem.Checked);
            this.showMsgItem["ShowLifePodsToolStripMenuItem"] = this.ShowLifePodsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowPopulationsWithoutGovernorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowPopulationsWithoutGovernorToolStripMenuItem.Name, this.ShowPopulationsWithoutGovernorToolStripMenuItem.Checked);
            this.showMsgItem["ShowPopulationsWithoutGovernorToolStripMenuItem"] = this.ShowPopulationsWithoutGovernorToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowOpenFireFCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowOpenFireFCToolStripMenuItem.Name, this.ShowOpenFireFCToolStripMenuItem.Checked);
            this.showMsgItem["ShowOpenFireFCToolStripMenuItem"] = this.ShowOpenFireFCToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowResearchFieldMismatchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowResearchFieldMismatchToolStripMenuItem.Name, this.ShowResearchFieldMismatchToolStripMenuItem.Checked);
            this.showMsgItem["ShowResearchFieldMismatchToolStripMenuItem"] = this.ShowResearchFieldMismatchToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowResearchWithoutResearchFacilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowResearchWithoutResearchFacilityToolStripMenuItem.Name, this.ShowResearchWithoutResearchFacilityToolStripMenuItem.Checked);
            this.showMsgItem["ShowResearchWithoutResearchFacilityToolStripMenuItem"] = this.ShowResearchWithoutResearchFacilityToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowSystemsWithUnsurveyedBodiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.Name, this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.Checked);
            this.showMsgItem["ShowSystemsWithUnsurveyedBodiesToolStripMenuItem"] = this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowIdleSoriumHarvestersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowIdleSoriumHarvestersToolStripMenuItem.Name, this.ShowIdleSoriumHarvestersToolStripMenuItem.Checked);
            this.showMsgItem["ShowIdleSoriumHarvestersToolStripMenuItem"] = this.ShowIdleSoriumHarvestersToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowIdleOrbitalMinersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowIdleOrbitalMinersToolStripMenuItem.Name, this.ShowIdleOrbitalMinersToolStripMenuItem.Checked);
            this.showMsgItem["ShowIdleOrbitalMinersToolStripMenuItem"] = this.ShowIdleOrbitalMinersToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.Name, this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.Checked);
            this.showMsgItem["ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem"] = this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowIdleGeosurveyFormationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowIdleGeosurveyFormationsToolStripMenuItem.Name, this.ShowIdleGeosurveyFormationsToolStripMenuItem.Checked);
            this.showMsgItem["ShowIdleGeosurveyFormationsToolStripMenuItem"] = this.ShowIdleGeosurveyFormationsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowDormantAncientConstructsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowDormantAncientConstructsToolStripMenuItem.Name, this.ShowDormantAncientConstructsToolStripMenuItem.Checked);
            this.showMsgItem["ShowDormantAncientConstructsToolStripMenuItem"] = this.ShowDormantAncientConstructsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowNotActiveAncientConstructsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowNotActiveAncientConstructsToolStripMenuItem.Name, this.ShowNotActiveAncientConstructsToolStripMenuItem.Checked);
            this.showMsgItem["ShowNotActiveAncientConstructsToolStripMenuItem"] = this.ShowNotActiveAncientConstructsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowHostileShipContactsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowHostileShipContactsToolStripMenuItem.Name, this.ShowHostileShipContactsToolStripMenuItem.Checked);
            this.showMsgItem["ShowHostileShipContactsToolStripMenuItem"] = this.ShowHostileShipContactsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowHostileGroundForceContactsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowHostileGroundForceContactsToolStripMenuItem.Name, this.ShowHostileGroundForceContactsToolStripMenuItem.Checked);
            this.showMsgItem["ShowHostileGroundForceContactsToolStripMenuItem"] = this.ShowHostileGroundForceContactsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowIdleShipyardsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowIdleShipyardsToolStripMenuItem.Name, this.ShowIdleShipyardsToolStripMenuItem.Checked);
            this.showMsgItem["ShowIdleShipyardsToolStripMenuItem"] = this.ShowIdleShipyardsToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private void ShowIdleGroundForceConstructionComplexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.controller.SetMenuItemState(this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.Name, this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.Checked);
            this.showMsgItem["ShowIdleGroundForceConstructionComplexToolStripMenuItem"] = this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.Checked;
            this.RefreshOverviewMessage();
        }

        private DataTable AdjustResourceDataTime(DataTable res)
        {
            res.Columns.Add("DateTime", typeof(DateTime));

            foreach (DataRow row in res.Rows)
            {
                int startyear;

                if (this.startYear < 100)
                {
                    startyear = this.startYear + (100 - this.startYear);
                }
                else
                {
                    startyear = this.startYear;
                }

                DateTime dtDateTime = new DateTime(startyear, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Local);
                DateTime dt = dtDateTime.AddSeconds(long.Parse(row["GameTime"].ToString())).ToLocalTime();
                row["DateTime"] = dt;
            }

            return res;
        }

        private void DisplayStartingYearWorkaroundWarning(bool enabled)
        {
            this.MineralOverviewDateWarningLabel.Visible = enabled;
            this.MineralPopulationDateWarningLabel.Visible = enabled;
            this.FuelDateWarningLabel.Visible = enabled;
            this.MaintenanceDateWarningLabel.Visible = enabled;
            this.PopulationDateWarningLabel.Visible = enabled;
            this.WealthDateWarningLabel.Visible = enabled;
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
$@"AuroraMarvin v{this.marvinVersion} for Aurora 4x C# {AURORAVERSION}

Questions, bugs, suggestions?
Please head over to the Aurora 4x forum.",
"AuroraMarvin",
MessageBoxButtons.OK,
MessageBoxIcon.Information,
MessageBoxDefaultButton.Button1,
0,
FORUMURL,
"Forum");
        }

        private void OverviewList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                string s = this.overviewList.SelectedItem.ToString();
                Clipboard.SetData(DataFormats.StringFormat, s);
            }
        }

        private void PopulationComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = this.controller.GetPopulationResources(int.Parse(this.PopulationComboBox1.SelectedValue.ToString()));

            this.chart6.DataSource = this.AdjustResourceDataTime(dt);
            this.chart6.DataBind();
            this.chart6.Update();
        }
    }
}
