﻿
namespace AuroraMarvin
{
    partial class AuroraMarvinView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series23 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series24 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series25 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series26 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuroraMarvinView));
            this.gameSelector = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.raceSelector = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.overviewList = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.populationViewer = new System.Windows.Forms.DataGridView();
            this.popNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.academyOfficersDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.capitalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.terraformStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchaseCivilianMineralsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colonistDestinationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.efficiencyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fighterDestFleetIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fuelProdStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fuelStockpileDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genModSpeciesIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groundAttackIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groundAttackTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastColonyCostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maintenanceStockpileDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maintProdStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.massDriverDestDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxAtmDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noStatusChangeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.politicalStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.populationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.previousUnrestDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provideColonistsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reqInfDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusPointsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.systemIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.systemBodyIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tempMFDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.terraformingGasIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unrestPointsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.duraniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.neutroniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.corbomiteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tritaniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boronideDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mercassiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vendariteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soriumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uridiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.corundiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.galliciteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastDuraniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNeutroniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastCorbomiteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastTritaniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastBoronideDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastMercassiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastVendariteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastSoriumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastUridiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastCorundiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastGalliciteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveDuraniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveNeutroniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveCorbomiteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveTritaniumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveBoronideDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveMercassiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveVendariteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveSoriumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveUridiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveCorundiumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reserveGalliciteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groundGeoSurveyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.destroyedInstallationSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aIValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invasionStagingPointDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.originalRaceIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doNotDeleteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.militaryRestrictedColonyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.academyNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spaceStationDestFleetIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.autoAssignDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bonusOneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bonusTwoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bonusThreeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fCTPopulationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.shipViewer = new System.Windows.Forms.DataGridView();
            this.shipNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fleetIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subFleetIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activeSensorsOnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedMSIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.autofireDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boardingCombatClockDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.constructedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.crewMoraleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentCrewDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentShieldStrengthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.damageControlIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.destroyedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fireDelayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fuelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gradePointsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.holdTechDataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.killTonnageCommercialDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.killTonnageMilitaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastLaunchTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastOverhaulDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastShoreLeaveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.launchMoraleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maintenanceStateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mothershipIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.raceIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refuelPriorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refuelStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scrapFlagDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sensorDelayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shieldsActiveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipClassIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipFuelEfficiencyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipNotesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shippingLineIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speciesIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.squadronIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.syncFireDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tFPointsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transponderActiveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ordnanceTransferStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hangarLoadTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resupplyPriorityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentMaintSuppliesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.automatedDamageControlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tractorTargetShipIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tractorTargetShipyardIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tractorParentShipIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.overhaulFactorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bioEnergyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastMissileHitTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastBeamHitTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastDamageTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastPenetratingDamageTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedFormationIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fCTShipBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.shipName = new System.Windows.Forms.TextBox();
            this.saveDesignButton = new System.Windows.Forms.Button();
            this.shipDisplay = new System.Windows.Forms.RichTextBox();
            this.shipDesignSelector = new System.Windows.Forms.ComboBox();
            this.hullSelector = new System.Windows.Forms.ComboBox();
            this.fCTHullDescriptionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.notesBox = new System.Windows.Forms.RichTextBox();
            this.notesSaveButton = new System.Windows.Forms.Button();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.GameNotesSaveButton = new System.Windows.Forms.Button();
            this.gameNotesBox = new System.Windows.Forms.RichTextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.MineralOverviewDateWarningLabel = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.mineralChangesGridView = new System.Windows.Forms.DataGridView();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.MineralPopulationDateWarningLabel = new System.Windows.Forms.Label();
            this.PopulationComboBox1 = new System.Windows.Forms.ComboBox();
            this.chart6 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.FuelDateWarningLabel = new System.Windows.Forms.Label();
            this.fuelTankerLabel = new System.Windows.Forms.Label();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.MaintenanceDateWarningLabel = new System.Windows.Forms.Label();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.PopulationDateWarningLabel = new System.Windows.Forms.Label();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.WealthDateWarningLabel = new System.Windows.Forms.Label();
            this.chart5 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.eventColourView = new System.Windows.Forms.DataGridView();
            this.fileButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.GameTimeLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowTechTreeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowDamagedShipsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowLowCrewMoraleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowFreeConstructionFactoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowFreeFighterFactoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowObsoleteShipsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowUnusedTerraformersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowUnusedMinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowShipsWithArmorDamageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowWrecksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowObsoleteTooledShipyardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowMissingSectorCommandersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowShipWithoutMspToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowShipWithLowMspToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowMissingAdminCommandersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowTaxedCivMinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowLowPopulationEfficienyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowLifePodsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowPopulationsWithoutGovernorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowOpenFireFCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowResearchFieldMismatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowIdleSoriumHarvestersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowIdleOrbitalMinersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowIdleGeosurveyFormationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowDormantAncientConstructsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowNotActiveAncientConstructsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowHostileShipContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowHostileGroundForceContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowIdleShipyardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.populationViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fCTPopulationBindingSource)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shipViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fCTShipBindingSource)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fCTHullDescriptionBindingSource)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mineralChangesGridView)).BeginInit();
            this.tabPage17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eventColourView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gameSelector
            // 
            this.gameSelector.FormattingEnabled = true;
            this.gameSelector.Location = new System.Drawing.Point(211, 29);
            this.gameSelector.Name = "gameSelector";
            this.gameSelector.Size = new System.Drawing.Size(121, 21);
            this.gameSelector.TabIndex = 7;
            this.gameSelector.SelectedIndexChanged += new System.EventHandler(this.GameSelector_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(137, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Select Game";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(338, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Select Race";
            // 
            // raceSelector
            // 
            this.raceSelector.FormattingEnabled = true;
            this.raceSelector.Location = new System.Drawing.Point(410, 29);
            this.raceSelector.Name = "raceSelector";
            this.raceSelector.Size = new System.Drawing.Size(121, 21);
            this.raceSelector.TabIndex = 10;
            this.raceSelector.SelectedIndexChanged += new System.EventHandler(this.RaceSelector_SelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Location = new System.Drawing.Point(12, 56);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(776, 469);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 11;
            this.tabControl1.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.overviewList);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(768, 443);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Overview";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // overviewList
            // 
            this.overviewList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.overviewList.FormattingEnabled = true;
            this.overviewList.Location = new System.Drawing.Point(0, 0);
            this.overviewList.Name = "overviewList";
            this.overviewList.Size = new System.Drawing.Size(772, 446);
            this.overviewList.TabIndex = 0;
            this.overviewList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OverviewList_KeyDown);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.populationViewer);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(768, 443);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Populations";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // populationViewer
            // 
            this.populationViewer.AllowUserToAddRows = false;
            this.populationViewer.AllowUserToDeleteRows = false;
            this.populationViewer.AllowUserToOrderColumns = true;
            this.populationViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.populationViewer.AutoGenerateColumns = false;
            this.populationViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.populationViewer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.popNameDataGridViewTextBoxColumn,
            this.academyOfficersDataGridViewTextBoxColumn,
            this.capitalDataGridViewTextBoxColumn,
            this.terraformStatusDataGridViewTextBoxColumn,
            this.purchaseCivilianMineralsDataGridViewTextBoxColumn,
            this.colonistDestinationDataGridViewTextBoxColumn,
            this.efficiencyDataGridViewTextBoxColumn,
            this.fighterDestFleetIDDataGridViewTextBoxColumn,
            this.fuelProdStatusDataGridViewTextBoxColumn,
            this.fuelStockpileDataGridViewTextBoxColumn,
            this.genModSpeciesIDDataGridViewTextBoxColumn,
            this.groundAttackIDDataGridViewTextBoxColumn,
            this.groundAttackTypeDataGridViewTextBoxColumn,
            this.lastColonyCostDataGridViewTextBoxColumn,
            this.maintenanceStockpileDataGridViewTextBoxColumn,
            this.maintProdStatusDataGridViewTextBoxColumn,
            this.massDriverDestDataGridViewTextBoxColumn,
            this.maxAtmDataGridViewTextBoxColumn,
            this.noStatusChangeDataGridViewTextBoxColumn,
            this.politicalStatusDataGridViewTextBoxColumn,
            this.populationDataGridViewTextBoxColumn,
            this.previousUnrestDataGridViewTextBoxColumn,
            this.provideColonistsDataGridViewTextBoxColumn,
            this.reqInfDataGridViewTextBoxColumn,
            this.statusPointsDataGridViewTextBoxColumn,
            this.systemIDDataGridViewTextBoxColumn,
            this.systemBodyIDDataGridViewTextBoxColumn,
            this.tempMFDataGridViewTextBoxColumn,
            this.terraformingGasIDDataGridViewTextBoxColumn,
            this.unrestPointsDataGridViewTextBoxColumn,
            this.duraniumDataGridViewTextBoxColumn,
            this.neutroniumDataGridViewTextBoxColumn,
            this.corbomiteDataGridViewTextBoxColumn,
            this.tritaniumDataGridViewTextBoxColumn,
            this.boronideDataGridViewTextBoxColumn,
            this.mercassiumDataGridViewTextBoxColumn,
            this.vendariteDataGridViewTextBoxColumn,
            this.soriumDataGridViewTextBoxColumn,
            this.uridiumDataGridViewTextBoxColumn,
            this.corundiumDataGridViewTextBoxColumn,
            this.galliciteDataGridViewTextBoxColumn,
            this.lastDuraniumDataGridViewTextBoxColumn,
            this.lastNeutroniumDataGridViewTextBoxColumn,
            this.lastCorbomiteDataGridViewTextBoxColumn,
            this.lastTritaniumDataGridViewTextBoxColumn,
            this.lastBoronideDataGridViewTextBoxColumn,
            this.lastMercassiumDataGridViewTextBoxColumn,
            this.lastVendariteDataGridViewTextBoxColumn,
            this.lastSoriumDataGridViewTextBoxColumn,
            this.lastUridiumDataGridViewTextBoxColumn,
            this.lastCorundiumDataGridViewTextBoxColumn,
            this.lastGalliciteDataGridViewTextBoxColumn,
            this.reserveDuraniumDataGridViewTextBoxColumn,
            this.reserveNeutroniumDataGridViewTextBoxColumn,
            this.reserveCorbomiteDataGridViewTextBoxColumn,
            this.reserveTritaniumDataGridViewTextBoxColumn,
            this.reserveBoronideDataGridViewTextBoxColumn,
            this.reserveMercassiumDataGridViewTextBoxColumn,
            this.reserveVendariteDataGridViewTextBoxColumn,
            this.reserveSoriumDataGridViewTextBoxColumn,
            this.reserveUridiumDataGridViewTextBoxColumn,
            this.reserveCorundiumDataGridViewTextBoxColumn,
            this.reserveGalliciteDataGridViewTextBoxColumn,
            this.groundGeoSurveyDataGridViewTextBoxColumn,
            this.destroyedInstallationSizeDataGridViewTextBoxColumn,
            this.aIValueDataGridViewTextBoxColumn,
            this.invasionStagingPointDataGridViewTextBoxColumn,
            this.originalRaceIDDataGridViewTextBoxColumn,
            this.doNotDeleteDataGridViewTextBoxColumn,
            this.militaryRestrictedColonyDataGridViewTextBoxColumn,
            this.academyNameDataGridViewTextBoxColumn,
            this.spaceStationDestFleetIDDataGridViewTextBoxColumn,
            this.importanceDataGridViewTextBoxColumn,
            this.autoAssignDataGridViewTextBoxColumn,
            this.bonusOneDataGridViewTextBoxColumn,
            this.bonusTwoDataGridViewTextBoxColumn,
            this.bonusThreeDataGridViewTextBoxColumn});
            this.populationViewer.DataSource = this.fCTPopulationBindingSource;
            this.populationViewer.Location = new System.Drawing.Point(0, 0);
            this.populationViewer.Name = "populationViewer";
            this.populationViewer.ReadOnly = true;
            this.populationViewer.Size = new System.Drawing.Size(769, 443);
            this.populationViewer.TabIndex = 1;
            // 
            // popNameDataGridViewTextBoxColumn
            // 
            this.popNameDataGridViewTextBoxColumn.DataPropertyName = "PopName";
            this.popNameDataGridViewTextBoxColumn.HeaderText = "PopName";
            this.popNameDataGridViewTextBoxColumn.Name = "popNameDataGridViewTextBoxColumn";
            this.popNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // academyOfficersDataGridViewTextBoxColumn
            // 
            this.academyOfficersDataGridViewTextBoxColumn.DataPropertyName = "AcademyOfficers";
            this.academyOfficersDataGridViewTextBoxColumn.HeaderText = "AcademyOfficers";
            this.academyOfficersDataGridViewTextBoxColumn.Name = "academyOfficersDataGridViewTextBoxColumn";
            this.academyOfficersDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // capitalDataGridViewTextBoxColumn
            // 
            this.capitalDataGridViewTextBoxColumn.DataPropertyName = "Capital";
            this.capitalDataGridViewTextBoxColumn.HeaderText = "Capital";
            this.capitalDataGridViewTextBoxColumn.Name = "capitalDataGridViewTextBoxColumn";
            this.capitalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // terraformStatusDataGridViewTextBoxColumn
            // 
            this.terraformStatusDataGridViewTextBoxColumn.DataPropertyName = "TerraformStatus";
            this.terraformStatusDataGridViewTextBoxColumn.HeaderText = "TerraformStatus";
            this.terraformStatusDataGridViewTextBoxColumn.Name = "terraformStatusDataGridViewTextBoxColumn";
            this.terraformStatusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // purchaseCivilianMineralsDataGridViewTextBoxColumn
            // 
            this.purchaseCivilianMineralsDataGridViewTextBoxColumn.DataPropertyName = "PurchaseCivilianMinerals";
            this.purchaseCivilianMineralsDataGridViewTextBoxColumn.HeaderText = "PurchaseCivilianMinerals";
            this.purchaseCivilianMineralsDataGridViewTextBoxColumn.Name = "purchaseCivilianMineralsDataGridViewTextBoxColumn";
            this.purchaseCivilianMineralsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // colonistDestinationDataGridViewTextBoxColumn
            // 
            this.colonistDestinationDataGridViewTextBoxColumn.DataPropertyName = "ColonistDestination";
            this.colonistDestinationDataGridViewTextBoxColumn.HeaderText = "ColonistDestination";
            this.colonistDestinationDataGridViewTextBoxColumn.Name = "colonistDestinationDataGridViewTextBoxColumn";
            this.colonistDestinationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // efficiencyDataGridViewTextBoxColumn
            // 
            this.efficiencyDataGridViewTextBoxColumn.DataPropertyName = "Efficiency";
            this.efficiencyDataGridViewTextBoxColumn.HeaderText = "Efficiency";
            this.efficiencyDataGridViewTextBoxColumn.Name = "efficiencyDataGridViewTextBoxColumn";
            this.efficiencyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fighterDestFleetIDDataGridViewTextBoxColumn
            // 
            this.fighterDestFleetIDDataGridViewTextBoxColumn.DataPropertyName = "FighterDestFleetID";
            this.fighterDestFleetIDDataGridViewTextBoxColumn.HeaderText = "FighterDestFleetID";
            this.fighterDestFleetIDDataGridViewTextBoxColumn.Name = "fighterDestFleetIDDataGridViewTextBoxColumn";
            this.fighterDestFleetIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fuelProdStatusDataGridViewTextBoxColumn
            // 
            this.fuelProdStatusDataGridViewTextBoxColumn.DataPropertyName = "FuelProdStatus";
            this.fuelProdStatusDataGridViewTextBoxColumn.HeaderText = "FuelProdStatus";
            this.fuelProdStatusDataGridViewTextBoxColumn.Name = "fuelProdStatusDataGridViewTextBoxColumn";
            this.fuelProdStatusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fuelStockpileDataGridViewTextBoxColumn
            // 
            this.fuelStockpileDataGridViewTextBoxColumn.DataPropertyName = "FuelStockpile";
            this.fuelStockpileDataGridViewTextBoxColumn.HeaderText = "FuelStockpile";
            this.fuelStockpileDataGridViewTextBoxColumn.Name = "fuelStockpileDataGridViewTextBoxColumn";
            this.fuelStockpileDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // genModSpeciesIDDataGridViewTextBoxColumn
            // 
            this.genModSpeciesIDDataGridViewTextBoxColumn.DataPropertyName = "GenModSpeciesID";
            this.genModSpeciesIDDataGridViewTextBoxColumn.HeaderText = "GenModSpeciesID";
            this.genModSpeciesIDDataGridViewTextBoxColumn.Name = "genModSpeciesIDDataGridViewTextBoxColumn";
            this.genModSpeciesIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // groundAttackIDDataGridViewTextBoxColumn
            // 
            this.groundAttackIDDataGridViewTextBoxColumn.DataPropertyName = "GroundAttackID";
            this.groundAttackIDDataGridViewTextBoxColumn.HeaderText = "GroundAttackID";
            this.groundAttackIDDataGridViewTextBoxColumn.Name = "groundAttackIDDataGridViewTextBoxColumn";
            this.groundAttackIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // groundAttackTypeDataGridViewTextBoxColumn
            // 
            this.groundAttackTypeDataGridViewTextBoxColumn.DataPropertyName = "GroundAttackType";
            this.groundAttackTypeDataGridViewTextBoxColumn.HeaderText = "GroundAttackType";
            this.groundAttackTypeDataGridViewTextBoxColumn.Name = "groundAttackTypeDataGridViewTextBoxColumn";
            this.groundAttackTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastColonyCostDataGridViewTextBoxColumn
            // 
            this.lastColonyCostDataGridViewTextBoxColumn.DataPropertyName = "LastColonyCost";
            this.lastColonyCostDataGridViewTextBoxColumn.HeaderText = "LastColonyCost";
            this.lastColonyCostDataGridViewTextBoxColumn.Name = "lastColonyCostDataGridViewTextBoxColumn";
            this.lastColonyCostDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maintenanceStockpileDataGridViewTextBoxColumn
            // 
            this.maintenanceStockpileDataGridViewTextBoxColumn.DataPropertyName = "MaintenanceStockpile";
            this.maintenanceStockpileDataGridViewTextBoxColumn.HeaderText = "MaintenanceStockpile";
            this.maintenanceStockpileDataGridViewTextBoxColumn.Name = "maintenanceStockpileDataGridViewTextBoxColumn";
            this.maintenanceStockpileDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maintProdStatusDataGridViewTextBoxColumn
            // 
            this.maintProdStatusDataGridViewTextBoxColumn.DataPropertyName = "MaintProdStatus";
            this.maintProdStatusDataGridViewTextBoxColumn.HeaderText = "MaintProdStatus";
            this.maintProdStatusDataGridViewTextBoxColumn.Name = "maintProdStatusDataGridViewTextBoxColumn";
            this.maintProdStatusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // massDriverDestDataGridViewTextBoxColumn
            // 
            this.massDriverDestDataGridViewTextBoxColumn.DataPropertyName = "MassDriverDest";
            this.massDriverDestDataGridViewTextBoxColumn.HeaderText = "MassDriverDest";
            this.massDriverDestDataGridViewTextBoxColumn.Name = "massDriverDestDataGridViewTextBoxColumn";
            this.massDriverDestDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maxAtmDataGridViewTextBoxColumn
            // 
            this.maxAtmDataGridViewTextBoxColumn.DataPropertyName = "MaxAtm";
            this.maxAtmDataGridViewTextBoxColumn.HeaderText = "MaxAtm";
            this.maxAtmDataGridViewTextBoxColumn.Name = "maxAtmDataGridViewTextBoxColumn";
            this.maxAtmDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // noStatusChangeDataGridViewTextBoxColumn
            // 
            this.noStatusChangeDataGridViewTextBoxColumn.DataPropertyName = "NoStatusChange";
            this.noStatusChangeDataGridViewTextBoxColumn.HeaderText = "NoStatusChange";
            this.noStatusChangeDataGridViewTextBoxColumn.Name = "noStatusChangeDataGridViewTextBoxColumn";
            this.noStatusChangeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // politicalStatusDataGridViewTextBoxColumn
            // 
            this.politicalStatusDataGridViewTextBoxColumn.DataPropertyName = "PoliticalStatus";
            this.politicalStatusDataGridViewTextBoxColumn.HeaderText = "PoliticalStatus";
            this.politicalStatusDataGridViewTextBoxColumn.Name = "politicalStatusDataGridViewTextBoxColumn";
            this.politicalStatusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // populationDataGridViewTextBoxColumn
            // 
            this.populationDataGridViewTextBoxColumn.DataPropertyName = "Population";
            this.populationDataGridViewTextBoxColumn.HeaderText = "Population";
            this.populationDataGridViewTextBoxColumn.Name = "populationDataGridViewTextBoxColumn";
            this.populationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // previousUnrestDataGridViewTextBoxColumn
            // 
            this.previousUnrestDataGridViewTextBoxColumn.DataPropertyName = "PreviousUnrest";
            this.previousUnrestDataGridViewTextBoxColumn.HeaderText = "PreviousUnrest";
            this.previousUnrestDataGridViewTextBoxColumn.Name = "previousUnrestDataGridViewTextBoxColumn";
            this.previousUnrestDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // provideColonistsDataGridViewTextBoxColumn
            // 
            this.provideColonistsDataGridViewTextBoxColumn.DataPropertyName = "ProvideColonists";
            this.provideColonistsDataGridViewTextBoxColumn.HeaderText = "ProvideColonists";
            this.provideColonistsDataGridViewTextBoxColumn.Name = "provideColonistsDataGridViewTextBoxColumn";
            this.provideColonistsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reqInfDataGridViewTextBoxColumn
            // 
            this.reqInfDataGridViewTextBoxColumn.DataPropertyName = "ReqInf";
            this.reqInfDataGridViewTextBoxColumn.HeaderText = "ReqInf";
            this.reqInfDataGridViewTextBoxColumn.Name = "reqInfDataGridViewTextBoxColumn";
            this.reqInfDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // statusPointsDataGridViewTextBoxColumn
            // 
            this.statusPointsDataGridViewTextBoxColumn.DataPropertyName = "StatusPoints";
            this.statusPointsDataGridViewTextBoxColumn.HeaderText = "StatusPoints";
            this.statusPointsDataGridViewTextBoxColumn.Name = "statusPointsDataGridViewTextBoxColumn";
            this.statusPointsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // systemIDDataGridViewTextBoxColumn
            // 
            this.systemIDDataGridViewTextBoxColumn.DataPropertyName = "SystemID";
            this.systemIDDataGridViewTextBoxColumn.HeaderText = "SystemID";
            this.systemIDDataGridViewTextBoxColumn.Name = "systemIDDataGridViewTextBoxColumn";
            this.systemIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // systemBodyIDDataGridViewTextBoxColumn
            // 
            this.systemBodyIDDataGridViewTextBoxColumn.DataPropertyName = "SystemBodyID";
            this.systemBodyIDDataGridViewTextBoxColumn.HeaderText = "SystemBodyID";
            this.systemBodyIDDataGridViewTextBoxColumn.Name = "systemBodyIDDataGridViewTextBoxColumn";
            this.systemBodyIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tempMFDataGridViewTextBoxColumn
            // 
            this.tempMFDataGridViewTextBoxColumn.DataPropertyName = "TempMF";
            this.tempMFDataGridViewTextBoxColumn.HeaderText = "TempMF";
            this.tempMFDataGridViewTextBoxColumn.Name = "tempMFDataGridViewTextBoxColumn";
            this.tempMFDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // terraformingGasIDDataGridViewTextBoxColumn
            // 
            this.terraformingGasIDDataGridViewTextBoxColumn.DataPropertyName = "TerraformingGasID";
            this.terraformingGasIDDataGridViewTextBoxColumn.HeaderText = "TerraformingGasID";
            this.terraformingGasIDDataGridViewTextBoxColumn.Name = "terraformingGasIDDataGridViewTextBoxColumn";
            this.terraformingGasIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // unrestPointsDataGridViewTextBoxColumn
            // 
            this.unrestPointsDataGridViewTextBoxColumn.DataPropertyName = "UnrestPoints";
            this.unrestPointsDataGridViewTextBoxColumn.HeaderText = "UnrestPoints";
            this.unrestPointsDataGridViewTextBoxColumn.Name = "unrestPointsDataGridViewTextBoxColumn";
            this.unrestPointsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // duraniumDataGridViewTextBoxColumn
            // 
            this.duraniumDataGridViewTextBoxColumn.DataPropertyName = "Duranium";
            this.duraniumDataGridViewTextBoxColumn.HeaderText = "Duranium";
            this.duraniumDataGridViewTextBoxColumn.Name = "duraniumDataGridViewTextBoxColumn";
            this.duraniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // neutroniumDataGridViewTextBoxColumn
            // 
            this.neutroniumDataGridViewTextBoxColumn.DataPropertyName = "Neutronium";
            this.neutroniumDataGridViewTextBoxColumn.HeaderText = "Neutronium";
            this.neutroniumDataGridViewTextBoxColumn.Name = "neutroniumDataGridViewTextBoxColumn";
            this.neutroniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // corbomiteDataGridViewTextBoxColumn
            // 
            this.corbomiteDataGridViewTextBoxColumn.DataPropertyName = "Corbomite";
            this.corbomiteDataGridViewTextBoxColumn.HeaderText = "Corbomite";
            this.corbomiteDataGridViewTextBoxColumn.Name = "corbomiteDataGridViewTextBoxColumn";
            this.corbomiteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tritaniumDataGridViewTextBoxColumn
            // 
            this.tritaniumDataGridViewTextBoxColumn.DataPropertyName = "Tritanium";
            this.tritaniumDataGridViewTextBoxColumn.HeaderText = "Tritanium";
            this.tritaniumDataGridViewTextBoxColumn.Name = "tritaniumDataGridViewTextBoxColumn";
            this.tritaniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // boronideDataGridViewTextBoxColumn
            // 
            this.boronideDataGridViewTextBoxColumn.DataPropertyName = "Boronide";
            this.boronideDataGridViewTextBoxColumn.HeaderText = "Boronide";
            this.boronideDataGridViewTextBoxColumn.Name = "boronideDataGridViewTextBoxColumn";
            this.boronideDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mercassiumDataGridViewTextBoxColumn
            // 
            this.mercassiumDataGridViewTextBoxColumn.DataPropertyName = "Mercassium";
            this.mercassiumDataGridViewTextBoxColumn.HeaderText = "Mercassium";
            this.mercassiumDataGridViewTextBoxColumn.Name = "mercassiumDataGridViewTextBoxColumn";
            this.mercassiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vendariteDataGridViewTextBoxColumn
            // 
            this.vendariteDataGridViewTextBoxColumn.DataPropertyName = "Vendarite";
            this.vendariteDataGridViewTextBoxColumn.HeaderText = "Vendarite";
            this.vendariteDataGridViewTextBoxColumn.Name = "vendariteDataGridViewTextBoxColumn";
            this.vendariteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // soriumDataGridViewTextBoxColumn
            // 
            this.soriumDataGridViewTextBoxColumn.DataPropertyName = "Sorium";
            this.soriumDataGridViewTextBoxColumn.HeaderText = "Sorium";
            this.soriumDataGridViewTextBoxColumn.Name = "soriumDataGridViewTextBoxColumn";
            this.soriumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uridiumDataGridViewTextBoxColumn
            // 
            this.uridiumDataGridViewTextBoxColumn.DataPropertyName = "Uridium";
            this.uridiumDataGridViewTextBoxColumn.HeaderText = "Uridium";
            this.uridiumDataGridViewTextBoxColumn.Name = "uridiumDataGridViewTextBoxColumn";
            this.uridiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // corundiumDataGridViewTextBoxColumn
            // 
            this.corundiumDataGridViewTextBoxColumn.DataPropertyName = "Corundium";
            this.corundiumDataGridViewTextBoxColumn.HeaderText = "Corundium";
            this.corundiumDataGridViewTextBoxColumn.Name = "corundiumDataGridViewTextBoxColumn";
            this.corundiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // galliciteDataGridViewTextBoxColumn
            // 
            this.galliciteDataGridViewTextBoxColumn.DataPropertyName = "Gallicite";
            this.galliciteDataGridViewTextBoxColumn.HeaderText = "Gallicite";
            this.galliciteDataGridViewTextBoxColumn.Name = "galliciteDataGridViewTextBoxColumn";
            this.galliciteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastDuraniumDataGridViewTextBoxColumn
            // 
            this.lastDuraniumDataGridViewTextBoxColumn.DataPropertyName = "LastDuranium";
            this.lastDuraniumDataGridViewTextBoxColumn.HeaderText = "LastDuranium";
            this.lastDuraniumDataGridViewTextBoxColumn.Name = "lastDuraniumDataGridViewTextBoxColumn";
            this.lastDuraniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastNeutroniumDataGridViewTextBoxColumn
            // 
            this.lastNeutroniumDataGridViewTextBoxColumn.DataPropertyName = "LastNeutronium";
            this.lastNeutroniumDataGridViewTextBoxColumn.HeaderText = "LastNeutronium";
            this.lastNeutroniumDataGridViewTextBoxColumn.Name = "lastNeutroniumDataGridViewTextBoxColumn";
            this.lastNeutroniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastCorbomiteDataGridViewTextBoxColumn
            // 
            this.lastCorbomiteDataGridViewTextBoxColumn.DataPropertyName = "LastCorbomite";
            this.lastCorbomiteDataGridViewTextBoxColumn.HeaderText = "LastCorbomite";
            this.lastCorbomiteDataGridViewTextBoxColumn.Name = "lastCorbomiteDataGridViewTextBoxColumn";
            this.lastCorbomiteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastTritaniumDataGridViewTextBoxColumn
            // 
            this.lastTritaniumDataGridViewTextBoxColumn.DataPropertyName = "LastTritanium";
            this.lastTritaniumDataGridViewTextBoxColumn.HeaderText = "LastTritanium";
            this.lastTritaniumDataGridViewTextBoxColumn.Name = "lastTritaniumDataGridViewTextBoxColumn";
            this.lastTritaniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastBoronideDataGridViewTextBoxColumn
            // 
            this.lastBoronideDataGridViewTextBoxColumn.DataPropertyName = "LastBoronide";
            this.lastBoronideDataGridViewTextBoxColumn.HeaderText = "LastBoronide";
            this.lastBoronideDataGridViewTextBoxColumn.Name = "lastBoronideDataGridViewTextBoxColumn";
            this.lastBoronideDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastMercassiumDataGridViewTextBoxColumn
            // 
            this.lastMercassiumDataGridViewTextBoxColumn.DataPropertyName = "LastMercassium";
            this.lastMercassiumDataGridViewTextBoxColumn.HeaderText = "LastMercassium";
            this.lastMercassiumDataGridViewTextBoxColumn.Name = "lastMercassiumDataGridViewTextBoxColumn";
            this.lastMercassiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastVendariteDataGridViewTextBoxColumn
            // 
            this.lastVendariteDataGridViewTextBoxColumn.DataPropertyName = "LastVendarite";
            this.lastVendariteDataGridViewTextBoxColumn.HeaderText = "LastVendarite";
            this.lastVendariteDataGridViewTextBoxColumn.Name = "lastVendariteDataGridViewTextBoxColumn";
            this.lastVendariteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastSoriumDataGridViewTextBoxColumn
            // 
            this.lastSoriumDataGridViewTextBoxColumn.DataPropertyName = "LastSorium";
            this.lastSoriumDataGridViewTextBoxColumn.HeaderText = "LastSorium";
            this.lastSoriumDataGridViewTextBoxColumn.Name = "lastSoriumDataGridViewTextBoxColumn";
            this.lastSoriumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastUridiumDataGridViewTextBoxColumn
            // 
            this.lastUridiumDataGridViewTextBoxColumn.DataPropertyName = "LastUridium";
            this.lastUridiumDataGridViewTextBoxColumn.HeaderText = "LastUridium";
            this.lastUridiumDataGridViewTextBoxColumn.Name = "lastUridiumDataGridViewTextBoxColumn";
            this.lastUridiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastCorundiumDataGridViewTextBoxColumn
            // 
            this.lastCorundiumDataGridViewTextBoxColumn.DataPropertyName = "LastCorundium";
            this.lastCorundiumDataGridViewTextBoxColumn.HeaderText = "LastCorundium";
            this.lastCorundiumDataGridViewTextBoxColumn.Name = "lastCorundiumDataGridViewTextBoxColumn";
            this.lastCorundiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastGalliciteDataGridViewTextBoxColumn
            // 
            this.lastGalliciteDataGridViewTextBoxColumn.DataPropertyName = "LastGallicite";
            this.lastGalliciteDataGridViewTextBoxColumn.HeaderText = "LastGallicite";
            this.lastGalliciteDataGridViewTextBoxColumn.Name = "lastGalliciteDataGridViewTextBoxColumn";
            this.lastGalliciteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveDuraniumDataGridViewTextBoxColumn
            // 
            this.reserveDuraniumDataGridViewTextBoxColumn.DataPropertyName = "ReserveDuranium";
            this.reserveDuraniumDataGridViewTextBoxColumn.HeaderText = "ReserveDuranium";
            this.reserveDuraniumDataGridViewTextBoxColumn.Name = "reserveDuraniumDataGridViewTextBoxColumn";
            this.reserveDuraniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveNeutroniumDataGridViewTextBoxColumn
            // 
            this.reserveNeutroniumDataGridViewTextBoxColumn.DataPropertyName = "ReserveNeutronium";
            this.reserveNeutroniumDataGridViewTextBoxColumn.HeaderText = "ReserveNeutronium";
            this.reserveNeutroniumDataGridViewTextBoxColumn.Name = "reserveNeutroniumDataGridViewTextBoxColumn";
            this.reserveNeutroniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveCorbomiteDataGridViewTextBoxColumn
            // 
            this.reserveCorbomiteDataGridViewTextBoxColumn.DataPropertyName = "ReserveCorbomite";
            this.reserveCorbomiteDataGridViewTextBoxColumn.HeaderText = "ReserveCorbomite";
            this.reserveCorbomiteDataGridViewTextBoxColumn.Name = "reserveCorbomiteDataGridViewTextBoxColumn";
            this.reserveCorbomiteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveTritaniumDataGridViewTextBoxColumn
            // 
            this.reserveTritaniumDataGridViewTextBoxColumn.DataPropertyName = "ReserveTritanium";
            this.reserveTritaniumDataGridViewTextBoxColumn.HeaderText = "ReserveTritanium";
            this.reserveTritaniumDataGridViewTextBoxColumn.Name = "reserveTritaniumDataGridViewTextBoxColumn";
            this.reserveTritaniumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveBoronideDataGridViewTextBoxColumn
            // 
            this.reserveBoronideDataGridViewTextBoxColumn.DataPropertyName = "ReserveBoronide";
            this.reserveBoronideDataGridViewTextBoxColumn.HeaderText = "ReserveBoronide";
            this.reserveBoronideDataGridViewTextBoxColumn.Name = "reserveBoronideDataGridViewTextBoxColumn";
            this.reserveBoronideDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveMercassiumDataGridViewTextBoxColumn
            // 
            this.reserveMercassiumDataGridViewTextBoxColumn.DataPropertyName = "ReserveMercassium";
            this.reserveMercassiumDataGridViewTextBoxColumn.HeaderText = "ReserveMercassium";
            this.reserveMercassiumDataGridViewTextBoxColumn.Name = "reserveMercassiumDataGridViewTextBoxColumn";
            this.reserveMercassiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveVendariteDataGridViewTextBoxColumn
            // 
            this.reserveVendariteDataGridViewTextBoxColumn.DataPropertyName = "ReserveVendarite";
            this.reserveVendariteDataGridViewTextBoxColumn.HeaderText = "ReserveVendarite";
            this.reserveVendariteDataGridViewTextBoxColumn.Name = "reserveVendariteDataGridViewTextBoxColumn";
            this.reserveVendariteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveSoriumDataGridViewTextBoxColumn
            // 
            this.reserveSoriumDataGridViewTextBoxColumn.DataPropertyName = "ReserveSorium";
            this.reserveSoriumDataGridViewTextBoxColumn.HeaderText = "ReserveSorium";
            this.reserveSoriumDataGridViewTextBoxColumn.Name = "reserveSoriumDataGridViewTextBoxColumn";
            this.reserveSoriumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveUridiumDataGridViewTextBoxColumn
            // 
            this.reserveUridiumDataGridViewTextBoxColumn.DataPropertyName = "ReserveUridium";
            this.reserveUridiumDataGridViewTextBoxColumn.HeaderText = "ReserveUridium";
            this.reserveUridiumDataGridViewTextBoxColumn.Name = "reserveUridiumDataGridViewTextBoxColumn";
            this.reserveUridiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveCorundiumDataGridViewTextBoxColumn
            // 
            this.reserveCorundiumDataGridViewTextBoxColumn.DataPropertyName = "ReserveCorundium";
            this.reserveCorundiumDataGridViewTextBoxColumn.HeaderText = "ReserveCorundium";
            this.reserveCorundiumDataGridViewTextBoxColumn.Name = "reserveCorundiumDataGridViewTextBoxColumn";
            this.reserveCorundiumDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // reserveGalliciteDataGridViewTextBoxColumn
            // 
            this.reserveGalliciteDataGridViewTextBoxColumn.DataPropertyName = "ReserveGallicite";
            this.reserveGalliciteDataGridViewTextBoxColumn.HeaderText = "ReserveGallicite";
            this.reserveGalliciteDataGridViewTextBoxColumn.Name = "reserveGalliciteDataGridViewTextBoxColumn";
            this.reserveGalliciteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // groundGeoSurveyDataGridViewTextBoxColumn
            // 
            this.groundGeoSurveyDataGridViewTextBoxColumn.DataPropertyName = "GroundGeoSurvey";
            this.groundGeoSurveyDataGridViewTextBoxColumn.HeaderText = "GroundGeoSurvey";
            this.groundGeoSurveyDataGridViewTextBoxColumn.Name = "groundGeoSurveyDataGridViewTextBoxColumn";
            this.groundGeoSurveyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // destroyedInstallationSizeDataGridViewTextBoxColumn
            // 
            this.destroyedInstallationSizeDataGridViewTextBoxColumn.DataPropertyName = "DestroyedInstallationSize";
            this.destroyedInstallationSizeDataGridViewTextBoxColumn.HeaderText = "DestroyedInstallationSize";
            this.destroyedInstallationSizeDataGridViewTextBoxColumn.Name = "destroyedInstallationSizeDataGridViewTextBoxColumn";
            this.destroyedInstallationSizeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // aIValueDataGridViewTextBoxColumn
            // 
            this.aIValueDataGridViewTextBoxColumn.DataPropertyName = "AIValue";
            this.aIValueDataGridViewTextBoxColumn.HeaderText = "AIValue";
            this.aIValueDataGridViewTextBoxColumn.Name = "aIValueDataGridViewTextBoxColumn";
            this.aIValueDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // invasionStagingPointDataGridViewTextBoxColumn
            // 
            this.invasionStagingPointDataGridViewTextBoxColumn.DataPropertyName = "InvasionStagingPoint";
            this.invasionStagingPointDataGridViewTextBoxColumn.HeaderText = "InvasionStagingPoint";
            this.invasionStagingPointDataGridViewTextBoxColumn.Name = "invasionStagingPointDataGridViewTextBoxColumn";
            this.invasionStagingPointDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // originalRaceIDDataGridViewTextBoxColumn
            // 
            this.originalRaceIDDataGridViewTextBoxColumn.DataPropertyName = "OriginalRaceID";
            this.originalRaceIDDataGridViewTextBoxColumn.HeaderText = "OriginalRaceID";
            this.originalRaceIDDataGridViewTextBoxColumn.Name = "originalRaceIDDataGridViewTextBoxColumn";
            this.originalRaceIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // doNotDeleteDataGridViewTextBoxColumn
            // 
            this.doNotDeleteDataGridViewTextBoxColumn.DataPropertyName = "DoNotDelete";
            this.doNotDeleteDataGridViewTextBoxColumn.HeaderText = "DoNotDelete";
            this.doNotDeleteDataGridViewTextBoxColumn.Name = "doNotDeleteDataGridViewTextBoxColumn";
            this.doNotDeleteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // militaryRestrictedColonyDataGridViewTextBoxColumn
            // 
            this.militaryRestrictedColonyDataGridViewTextBoxColumn.DataPropertyName = "MilitaryRestrictedColony";
            this.militaryRestrictedColonyDataGridViewTextBoxColumn.HeaderText = "MilitaryRestrictedColony";
            this.militaryRestrictedColonyDataGridViewTextBoxColumn.Name = "militaryRestrictedColonyDataGridViewTextBoxColumn";
            this.militaryRestrictedColonyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // academyNameDataGridViewTextBoxColumn
            // 
            this.academyNameDataGridViewTextBoxColumn.DataPropertyName = "AcademyName";
            this.academyNameDataGridViewTextBoxColumn.HeaderText = "AcademyName";
            this.academyNameDataGridViewTextBoxColumn.Name = "academyNameDataGridViewTextBoxColumn";
            this.academyNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // spaceStationDestFleetIDDataGridViewTextBoxColumn
            // 
            this.spaceStationDestFleetIDDataGridViewTextBoxColumn.DataPropertyName = "SpaceStationDestFleetID";
            this.spaceStationDestFleetIDDataGridViewTextBoxColumn.HeaderText = "SpaceStationDestFleetID";
            this.spaceStationDestFleetIDDataGridViewTextBoxColumn.Name = "spaceStationDestFleetIDDataGridViewTextBoxColumn";
            this.spaceStationDestFleetIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // importanceDataGridViewTextBoxColumn
            // 
            this.importanceDataGridViewTextBoxColumn.DataPropertyName = "Importance";
            this.importanceDataGridViewTextBoxColumn.HeaderText = "Importance";
            this.importanceDataGridViewTextBoxColumn.Name = "importanceDataGridViewTextBoxColumn";
            this.importanceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // autoAssignDataGridViewTextBoxColumn
            // 
            this.autoAssignDataGridViewTextBoxColumn.DataPropertyName = "AutoAssign";
            this.autoAssignDataGridViewTextBoxColumn.HeaderText = "AutoAssign";
            this.autoAssignDataGridViewTextBoxColumn.Name = "autoAssignDataGridViewTextBoxColumn";
            this.autoAssignDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bonusOneDataGridViewTextBoxColumn
            // 
            this.bonusOneDataGridViewTextBoxColumn.DataPropertyName = "BonusOne";
            this.bonusOneDataGridViewTextBoxColumn.HeaderText = "BonusOne";
            this.bonusOneDataGridViewTextBoxColumn.Name = "bonusOneDataGridViewTextBoxColumn";
            this.bonusOneDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bonusTwoDataGridViewTextBoxColumn
            // 
            this.bonusTwoDataGridViewTextBoxColumn.DataPropertyName = "BonusTwo";
            this.bonusTwoDataGridViewTextBoxColumn.HeaderText = "BonusTwo";
            this.bonusTwoDataGridViewTextBoxColumn.Name = "bonusTwoDataGridViewTextBoxColumn";
            this.bonusTwoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bonusThreeDataGridViewTextBoxColumn
            // 
            this.bonusThreeDataGridViewTextBoxColumn.DataPropertyName = "BonusThree";
            this.bonusThreeDataGridViewTextBoxColumn.HeaderText = "BonusThree";
            this.bonusThreeDataGridViewTextBoxColumn.Name = "bonusThreeDataGridViewTextBoxColumn";
            this.bonusThreeDataGridViewTextBoxColumn.ReadOnly = true;
            this.bonusThreeDataGridViewTextBoxColumn.Width = 9;
            // 
            // fCTPopulationBindingSource
            // 
            this.fCTPopulationBindingSource.DataSource = typeof(AuroraMarvin.FCT_Population);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.shipViewer);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(768, 443);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Ships";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // shipViewer
            // 
            this.shipViewer.AllowUserToAddRows = false;
            this.shipViewer.AllowUserToDeleteRows = false;
            this.shipViewer.AllowUserToOrderColumns = true;
            this.shipViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.shipViewer.AutoGenerateColumns = false;
            this.shipViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.shipViewer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.shipNameDataGridViewTextBoxColumn,
            this.fleetIDDataGridViewTextBoxColumn,
            this.subFleetIDDataGridViewTextBoxColumn,
            this.activeSensorsOnDataGridViewTextBoxColumn,
            this.assignedMSIDDataGridViewTextBoxColumn,
            this.autofireDataGridViewTextBoxColumn,
            this.boardingCombatClockDataGridViewTextBoxColumn,
            this.constructedDataGridViewTextBoxColumn,
            this.crewMoraleDataGridViewTextBoxColumn,
            this.currentCrewDataGridViewTextBoxColumn,
            this.currentShieldStrengthDataGridViewTextBoxColumn,
            this.damageControlIDDataGridViewTextBoxColumn,
            this.destroyedDataGridViewTextBoxColumn,
            this.fireDelayDataGridViewTextBoxColumn,
            this.fuelDataGridViewTextBoxColumn,
            this.gradePointsDataGridViewTextBoxColumn,
            this.holdTechDataDataGridViewTextBoxColumn,
            this.killTonnageCommercialDataGridViewTextBoxColumn,
            this.killTonnageMilitaryDataGridViewTextBoxColumn,
            this.lastLaunchTimeDataGridViewTextBoxColumn,
            this.lastOverhaulDataGridViewTextBoxColumn,
            this.lastShoreLeaveDataGridViewTextBoxColumn,
            this.launchMoraleDataGridViewTextBoxColumn,
            this.maintenanceStateDataGridViewTextBoxColumn,
            this.mothershipIDDataGridViewTextBoxColumn,
            this.raceIDDataGridViewTextBoxColumn,
            this.refuelPriorityDataGridViewTextBoxColumn,
            this.refuelStatusDataGridViewTextBoxColumn,
            this.scrapFlagDataGridViewTextBoxColumn,
            this.sensorDelayDataGridViewTextBoxColumn,
            this.shieldsActiveDataGridViewTextBoxColumn,
            this.shipClassIDDataGridViewTextBoxColumn,
            this.shipFuelEfficiencyDataGridViewTextBoxColumn,
            this.shipNotesDataGridViewTextBoxColumn,
            this.shippingLineIDDataGridViewTextBoxColumn,
            this.speciesIDDataGridViewTextBoxColumn1,
            this.squadronIDDataGridViewTextBoxColumn,
            this.syncFireDataGridViewTextBoxColumn,
            this.tFPointsDataGridViewTextBoxColumn,
            this.transponderActiveDataGridViewTextBoxColumn,
            this.ordnanceTransferStatusDataGridViewTextBoxColumn,
            this.hangarLoadTypeDataGridViewTextBoxColumn,
            this.resupplyPriorityDataGridViewTextBoxColumn,
            this.currentMaintSuppliesDataGridViewTextBoxColumn,
            this.automatedDamageControlDataGridViewTextBoxColumn,
            this.tractorTargetShipIDDataGridViewTextBoxColumn,
            this.tractorTargetShipyardIDDataGridViewTextBoxColumn,
            this.tractorParentShipIDDataGridViewTextBoxColumn,
            this.overhaulFactorDataGridViewTextBoxColumn,
            this.bioEnergyDataGridViewTextBoxColumn,
            this.lastMissileHitTimeDataGridViewTextBoxColumn,
            this.lastBeamHitTimeDataGridViewTextBoxColumn,
            this.lastDamageTimeDataGridViewTextBoxColumn,
            this.lastPenetratingDamageTimeDataGridViewTextBoxColumn,
            this.assignedFormationIDDataGridViewTextBoxColumn});
            this.shipViewer.DataSource = this.fCTShipBindingSource;
            this.shipViewer.Location = new System.Drawing.Point(0, 0);
            this.shipViewer.Name = "shipViewer";
            this.shipViewer.ReadOnly = true;
            this.shipViewer.Size = new System.Drawing.Size(772, 443);
            this.shipViewer.TabIndex = 0;
            // 
            // shipNameDataGridViewTextBoxColumn
            // 
            this.shipNameDataGridViewTextBoxColumn.DataPropertyName = "ShipName";
            this.shipNameDataGridViewTextBoxColumn.HeaderText = "ShipName";
            this.shipNameDataGridViewTextBoxColumn.Name = "shipNameDataGridViewTextBoxColumn";
            this.shipNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fleetIDDataGridViewTextBoxColumn
            // 
            this.fleetIDDataGridViewTextBoxColumn.DataPropertyName = "FleetID";
            this.fleetIDDataGridViewTextBoxColumn.HeaderText = "FleetID";
            this.fleetIDDataGridViewTextBoxColumn.Name = "fleetIDDataGridViewTextBoxColumn";
            this.fleetIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // subFleetIDDataGridViewTextBoxColumn
            // 
            this.subFleetIDDataGridViewTextBoxColumn.DataPropertyName = "SubFleetID";
            this.subFleetIDDataGridViewTextBoxColumn.HeaderText = "SubFleetID";
            this.subFleetIDDataGridViewTextBoxColumn.Name = "subFleetIDDataGridViewTextBoxColumn";
            this.subFleetIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // activeSensorsOnDataGridViewTextBoxColumn
            // 
            this.activeSensorsOnDataGridViewTextBoxColumn.DataPropertyName = "ActiveSensorsOn";
            this.activeSensorsOnDataGridViewTextBoxColumn.HeaderText = "ActiveSensorsOn";
            this.activeSensorsOnDataGridViewTextBoxColumn.Name = "activeSensorsOnDataGridViewTextBoxColumn";
            this.activeSensorsOnDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // assignedMSIDDataGridViewTextBoxColumn
            // 
            this.assignedMSIDDataGridViewTextBoxColumn.DataPropertyName = "AssignedMSID";
            this.assignedMSIDDataGridViewTextBoxColumn.HeaderText = "AssignedMSID";
            this.assignedMSIDDataGridViewTextBoxColumn.Name = "assignedMSIDDataGridViewTextBoxColumn";
            this.assignedMSIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // autofireDataGridViewTextBoxColumn
            // 
            this.autofireDataGridViewTextBoxColumn.DataPropertyName = "Autofire";
            this.autofireDataGridViewTextBoxColumn.HeaderText = "Autofire";
            this.autofireDataGridViewTextBoxColumn.Name = "autofireDataGridViewTextBoxColumn";
            this.autofireDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // boardingCombatClockDataGridViewTextBoxColumn
            // 
            this.boardingCombatClockDataGridViewTextBoxColumn.DataPropertyName = "BoardingCombatClock";
            this.boardingCombatClockDataGridViewTextBoxColumn.HeaderText = "BoardingCombatClock";
            this.boardingCombatClockDataGridViewTextBoxColumn.Name = "boardingCombatClockDataGridViewTextBoxColumn";
            this.boardingCombatClockDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // constructedDataGridViewTextBoxColumn
            // 
            this.constructedDataGridViewTextBoxColumn.DataPropertyName = "Constructed";
            this.constructedDataGridViewTextBoxColumn.HeaderText = "Constructed";
            this.constructedDataGridViewTextBoxColumn.Name = "constructedDataGridViewTextBoxColumn";
            this.constructedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // crewMoraleDataGridViewTextBoxColumn
            // 
            this.crewMoraleDataGridViewTextBoxColumn.DataPropertyName = "CrewMorale";
            this.crewMoraleDataGridViewTextBoxColumn.HeaderText = "CrewMorale";
            this.crewMoraleDataGridViewTextBoxColumn.Name = "crewMoraleDataGridViewTextBoxColumn";
            this.crewMoraleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // currentCrewDataGridViewTextBoxColumn
            // 
            this.currentCrewDataGridViewTextBoxColumn.DataPropertyName = "CurrentCrew";
            this.currentCrewDataGridViewTextBoxColumn.HeaderText = "CurrentCrew";
            this.currentCrewDataGridViewTextBoxColumn.Name = "currentCrewDataGridViewTextBoxColumn";
            this.currentCrewDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // currentShieldStrengthDataGridViewTextBoxColumn
            // 
            this.currentShieldStrengthDataGridViewTextBoxColumn.DataPropertyName = "CurrentShieldStrength";
            this.currentShieldStrengthDataGridViewTextBoxColumn.HeaderText = "CurrentShieldStrength";
            this.currentShieldStrengthDataGridViewTextBoxColumn.Name = "currentShieldStrengthDataGridViewTextBoxColumn";
            this.currentShieldStrengthDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // damageControlIDDataGridViewTextBoxColumn
            // 
            this.damageControlIDDataGridViewTextBoxColumn.DataPropertyName = "DamageControlID";
            this.damageControlIDDataGridViewTextBoxColumn.HeaderText = "DamageControlID";
            this.damageControlIDDataGridViewTextBoxColumn.Name = "damageControlIDDataGridViewTextBoxColumn";
            this.damageControlIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // destroyedDataGridViewTextBoxColumn
            // 
            this.destroyedDataGridViewTextBoxColumn.DataPropertyName = "Destroyed";
            this.destroyedDataGridViewTextBoxColumn.HeaderText = "Destroyed";
            this.destroyedDataGridViewTextBoxColumn.Name = "destroyedDataGridViewTextBoxColumn";
            this.destroyedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fireDelayDataGridViewTextBoxColumn
            // 
            this.fireDelayDataGridViewTextBoxColumn.DataPropertyName = "FireDelay";
            this.fireDelayDataGridViewTextBoxColumn.HeaderText = "FireDelay";
            this.fireDelayDataGridViewTextBoxColumn.Name = "fireDelayDataGridViewTextBoxColumn";
            this.fireDelayDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fuelDataGridViewTextBoxColumn
            // 
            this.fuelDataGridViewTextBoxColumn.DataPropertyName = "Fuel";
            this.fuelDataGridViewTextBoxColumn.HeaderText = "Fuel";
            this.fuelDataGridViewTextBoxColumn.Name = "fuelDataGridViewTextBoxColumn";
            this.fuelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gradePointsDataGridViewTextBoxColumn
            // 
            this.gradePointsDataGridViewTextBoxColumn.DataPropertyName = "GradePoints";
            this.gradePointsDataGridViewTextBoxColumn.HeaderText = "GradePoints";
            this.gradePointsDataGridViewTextBoxColumn.Name = "gradePointsDataGridViewTextBoxColumn";
            this.gradePointsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // holdTechDataDataGridViewTextBoxColumn
            // 
            this.holdTechDataDataGridViewTextBoxColumn.DataPropertyName = "HoldTechData";
            this.holdTechDataDataGridViewTextBoxColumn.HeaderText = "HoldTechData";
            this.holdTechDataDataGridViewTextBoxColumn.Name = "holdTechDataDataGridViewTextBoxColumn";
            this.holdTechDataDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // killTonnageCommercialDataGridViewTextBoxColumn
            // 
            this.killTonnageCommercialDataGridViewTextBoxColumn.DataPropertyName = "KillTonnageCommercial";
            this.killTonnageCommercialDataGridViewTextBoxColumn.HeaderText = "KillTonnageCommercial";
            this.killTonnageCommercialDataGridViewTextBoxColumn.Name = "killTonnageCommercialDataGridViewTextBoxColumn";
            this.killTonnageCommercialDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // killTonnageMilitaryDataGridViewTextBoxColumn
            // 
            this.killTonnageMilitaryDataGridViewTextBoxColumn.DataPropertyName = "KillTonnageMilitary";
            this.killTonnageMilitaryDataGridViewTextBoxColumn.HeaderText = "KillTonnageMilitary";
            this.killTonnageMilitaryDataGridViewTextBoxColumn.Name = "killTonnageMilitaryDataGridViewTextBoxColumn";
            this.killTonnageMilitaryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastLaunchTimeDataGridViewTextBoxColumn
            // 
            this.lastLaunchTimeDataGridViewTextBoxColumn.DataPropertyName = "LastLaunchTime";
            this.lastLaunchTimeDataGridViewTextBoxColumn.HeaderText = "LastLaunchTime";
            this.lastLaunchTimeDataGridViewTextBoxColumn.Name = "lastLaunchTimeDataGridViewTextBoxColumn";
            this.lastLaunchTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastOverhaulDataGridViewTextBoxColumn
            // 
            this.lastOverhaulDataGridViewTextBoxColumn.DataPropertyName = "LastOverhaul";
            this.lastOverhaulDataGridViewTextBoxColumn.HeaderText = "LastOverhaul";
            this.lastOverhaulDataGridViewTextBoxColumn.Name = "lastOverhaulDataGridViewTextBoxColumn";
            this.lastOverhaulDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastShoreLeaveDataGridViewTextBoxColumn
            // 
            this.lastShoreLeaveDataGridViewTextBoxColumn.DataPropertyName = "LastShoreLeave";
            this.lastShoreLeaveDataGridViewTextBoxColumn.HeaderText = "LastShoreLeave";
            this.lastShoreLeaveDataGridViewTextBoxColumn.Name = "lastShoreLeaveDataGridViewTextBoxColumn";
            this.lastShoreLeaveDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // launchMoraleDataGridViewTextBoxColumn
            // 
            this.launchMoraleDataGridViewTextBoxColumn.DataPropertyName = "LaunchMorale";
            this.launchMoraleDataGridViewTextBoxColumn.HeaderText = "LaunchMorale";
            this.launchMoraleDataGridViewTextBoxColumn.Name = "launchMoraleDataGridViewTextBoxColumn";
            this.launchMoraleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maintenanceStateDataGridViewTextBoxColumn
            // 
            this.maintenanceStateDataGridViewTextBoxColumn.DataPropertyName = "MaintenanceState";
            this.maintenanceStateDataGridViewTextBoxColumn.HeaderText = "MaintenanceState";
            this.maintenanceStateDataGridViewTextBoxColumn.Name = "maintenanceStateDataGridViewTextBoxColumn";
            this.maintenanceStateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mothershipIDDataGridViewTextBoxColumn
            // 
            this.mothershipIDDataGridViewTextBoxColumn.DataPropertyName = "MothershipID";
            this.mothershipIDDataGridViewTextBoxColumn.HeaderText = "MothershipID";
            this.mothershipIDDataGridViewTextBoxColumn.Name = "mothershipIDDataGridViewTextBoxColumn";
            this.mothershipIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // raceIDDataGridViewTextBoxColumn
            // 
            this.raceIDDataGridViewTextBoxColumn.DataPropertyName = "RaceID";
            this.raceIDDataGridViewTextBoxColumn.HeaderText = "RaceID";
            this.raceIDDataGridViewTextBoxColumn.Name = "raceIDDataGridViewTextBoxColumn";
            this.raceIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // refuelPriorityDataGridViewTextBoxColumn
            // 
            this.refuelPriorityDataGridViewTextBoxColumn.DataPropertyName = "RefuelPriority";
            this.refuelPriorityDataGridViewTextBoxColumn.HeaderText = "RefuelPriority";
            this.refuelPriorityDataGridViewTextBoxColumn.Name = "refuelPriorityDataGridViewTextBoxColumn";
            this.refuelPriorityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // refuelStatusDataGridViewTextBoxColumn
            // 
            this.refuelStatusDataGridViewTextBoxColumn.DataPropertyName = "RefuelStatus";
            this.refuelStatusDataGridViewTextBoxColumn.HeaderText = "RefuelStatus";
            this.refuelStatusDataGridViewTextBoxColumn.Name = "refuelStatusDataGridViewTextBoxColumn";
            this.refuelStatusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // scrapFlagDataGridViewTextBoxColumn
            // 
            this.scrapFlagDataGridViewTextBoxColumn.DataPropertyName = "ScrapFlag";
            this.scrapFlagDataGridViewTextBoxColumn.HeaderText = "ScrapFlag";
            this.scrapFlagDataGridViewTextBoxColumn.Name = "scrapFlagDataGridViewTextBoxColumn";
            this.scrapFlagDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sensorDelayDataGridViewTextBoxColumn
            // 
            this.sensorDelayDataGridViewTextBoxColumn.DataPropertyName = "SensorDelay";
            this.sensorDelayDataGridViewTextBoxColumn.HeaderText = "SensorDelay";
            this.sensorDelayDataGridViewTextBoxColumn.Name = "sensorDelayDataGridViewTextBoxColumn";
            this.sensorDelayDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // shieldsActiveDataGridViewTextBoxColumn
            // 
            this.shieldsActiveDataGridViewTextBoxColumn.DataPropertyName = "ShieldsActive";
            this.shieldsActiveDataGridViewTextBoxColumn.HeaderText = "ShieldsActive";
            this.shieldsActiveDataGridViewTextBoxColumn.Name = "shieldsActiveDataGridViewTextBoxColumn";
            this.shieldsActiveDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // shipClassIDDataGridViewTextBoxColumn
            // 
            this.shipClassIDDataGridViewTextBoxColumn.DataPropertyName = "ShipClassID";
            this.shipClassIDDataGridViewTextBoxColumn.HeaderText = "ShipClassID";
            this.shipClassIDDataGridViewTextBoxColumn.Name = "shipClassIDDataGridViewTextBoxColumn";
            this.shipClassIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // shipFuelEfficiencyDataGridViewTextBoxColumn
            // 
            this.shipFuelEfficiencyDataGridViewTextBoxColumn.DataPropertyName = "ShipFuelEfficiency";
            this.shipFuelEfficiencyDataGridViewTextBoxColumn.HeaderText = "ShipFuelEfficiency";
            this.shipFuelEfficiencyDataGridViewTextBoxColumn.Name = "shipFuelEfficiencyDataGridViewTextBoxColumn";
            this.shipFuelEfficiencyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // shipNotesDataGridViewTextBoxColumn
            // 
            this.shipNotesDataGridViewTextBoxColumn.DataPropertyName = "ShipNotes";
            this.shipNotesDataGridViewTextBoxColumn.HeaderText = "ShipNotes";
            this.shipNotesDataGridViewTextBoxColumn.Name = "shipNotesDataGridViewTextBoxColumn";
            this.shipNotesDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // shippingLineIDDataGridViewTextBoxColumn
            // 
            this.shippingLineIDDataGridViewTextBoxColumn.DataPropertyName = "ShippingLineID";
            this.shippingLineIDDataGridViewTextBoxColumn.HeaderText = "ShippingLineID";
            this.shippingLineIDDataGridViewTextBoxColumn.Name = "shippingLineIDDataGridViewTextBoxColumn";
            this.shippingLineIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // speciesIDDataGridViewTextBoxColumn1
            // 
            this.speciesIDDataGridViewTextBoxColumn1.DataPropertyName = "SpeciesID";
            this.speciesIDDataGridViewTextBoxColumn1.HeaderText = "SpeciesID";
            this.speciesIDDataGridViewTextBoxColumn1.Name = "speciesIDDataGridViewTextBoxColumn1";
            this.speciesIDDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // squadronIDDataGridViewTextBoxColumn
            // 
            this.squadronIDDataGridViewTextBoxColumn.DataPropertyName = "SquadronID";
            this.squadronIDDataGridViewTextBoxColumn.HeaderText = "SquadronID";
            this.squadronIDDataGridViewTextBoxColumn.Name = "squadronIDDataGridViewTextBoxColumn";
            this.squadronIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // syncFireDataGridViewTextBoxColumn
            // 
            this.syncFireDataGridViewTextBoxColumn.DataPropertyName = "SyncFire";
            this.syncFireDataGridViewTextBoxColumn.HeaderText = "SyncFire";
            this.syncFireDataGridViewTextBoxColumn.Name = "syncFireDataGridViewTextBoxColumn";
            this.syncFireDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tFPointsDataGridViewTextBoxColumn
            // 
            this.tFPointsDataGridViewTextBoxColumn.DataPropertyName = "TFPoints";
            this.tFPointsDataGridViewTextBoxColumn.HeaderText = "TFPoints";
            this.tFPointsDataGridViewTextBoxColumn.Name = "tFPointsDataGridViewTextBoxColumn";
            this.tFPointsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // transponderActiveDataGridViewTextBoxColumn
            // 
            this.transponderActiveDataGridViewTextBoxColumn.DataPropertyName = "TransponderActive";
            this.transponderActiveDataGridViewTextBoxColumn.HeaderText = "TransponderActive";
            this.transponderActiveDataGridViewTextBoxColumn.Name = "transponderActiveDataGridViewTextBoxColumn";
            this.transponderActiveDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ordnanceTransferStatusDataGridViewTextBoxColumn
            // 
            this.ordnanceTransferStatusDataGridViewTextBoxColumn.DataPropertyName = "OrdnanceTransferStatus";
            this.ordnanceTransferStatusDataGridViewTextBoxColumn.HeaderText = "OrdnanceTransferStatus";
            this.ordnanceTransferStatusDataGridViewTextBoxColumn.Name = "ordnanceTransferStatusDataGridViewTextBoxColumn";
            this.ordnanceTransferStatusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hangarLoadTypeDataGridViewTextBoxColumn
            // 
            this.hangarLoadTypeDataGridViewTextBoxColumn.DataPropertyName = "HangarLoadType";
            this.hangarLoadTypeDataGridViewTextBoxColumn.HeaderText = "HangarLoadType";
            this.hangarLoadTypeDataGridViewTextBoxColumn.Name = "hangarLoadTypeDataGridViewTextBoxColumn";
            this.hangarLoadTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // resupplyPriorityDataGridViewTextBoxColumn
            // 
            this.resupplyPriorityDataGridViewTextBoxColumn.DataPropertyName = "ResupplyPriority";
            this.resupplyPriorityDataGridViewTextBoxColumn.HeaderText = "ResupplyPriority";
            this.resupplyPriorityDataGridViewTextBoxColumn.Name = "resupplyPriorityDataGridViewTextBoxColumn";
            this.resupplyPriorityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // currentMaintSuppliesDataGridViewTextBoxColumn
            // 
            this.currentMaintSuppliesDataGridViewTextBoxColumn.DataPropertyName = "CurrentMaintSupplies";
            this.currentMaintSuppliesDataGridViewTextBoxColumn.HeaderText = "CurrentMaintSupplies";
            this.currentMaintSuppliesDataGridViewTextBoxColumn.Name = "currentMaintSuppliesDataGridViewTextBoxColumn";
            this.currentMaintSuppliesDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // automatedDamageControlDataGridViewTextBoxColumn
            // 
            this.automatedDamageControlDataGridViewTextBoxColumn.DataPropertyName = "AutomatedDamageControl";
            this.automatedDamageControlDataGridViewTextBoxColumn.HeaderText = "AutomatedDamageControl";
            this.automatedDamageControlDataGridViewTextBoxColumn.Name = "automatedDamageControlDataGridViewTextBoxColumn";
            this.automatedDamageControlDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tractorTargetShipIDDataGridViewTextBoxColumn
            // 
            this.tractorTargetShipIDDataGridViewTextBoxColumn.DataPropertyName = "TractorTargetShipID";
            this.tractorTargetShipIDDataGridViewTextBoxColumn.HeaderText = "TractorTargetShipID";
            this.tractorTargetShipIDDataGridViewTextBoxColumn.Name = "tractorTargetShipIDDataGridViewTextBoxColumn";
            this.tractorTargetShipIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tractorTargetShipyardIDDataGridViewTextBoxColumn
            // 
            this.tractorTargetShipyardIDDataGridViewTextBoxColumn.DataPropertyName = "TractorTargetShipyardID";
            this.tractorTargetShipyardIDDataGridViewTextBoxColumn.HeaderText = "TractorTargetShipyardID";
            this.tractorTargetShipyardIDDataGridViewTextBoxColumn.Name = "tractorTargetShipyardIDDataGridViewTextBoxColumn";
            this.tractorTargetShipyardIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tractorParentShipIDDataGridViewTextBoxColumn
            // 
            this.tractorParentShipIDDataGridViewTextBoxColumn.DataPropertyName = "TractorParentShipID";
            this.tractorParentShipIDDataGridViewTextBoxColumn.HeaderText = "TractorParentShipID";
            this.tractorParentShipIDDataGridViewTextBoxColumn.Name = "tractorParentShipIDDataGridViewTextBoxColumn";
            this.tractorParentShipIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // overhaulFactorDataGridViewTextBoxColumn
            // 
            this.overhaulFactorDataGridViewTextBoxColumn.DataPropertyName = "OverhaulFactor";
            this.overhaulFactorDataGridViewTextBoxColumn.HeaderText = "OverhaulFactor";
            this.overhaulFactorDataGridViewTextBoxColumn.Name = "overhaulFactorDataGridViewTextBoxColumn";
            this.overhaulFactorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bioEnergyDataGridViewTextBoxColumn
            // 
            this.bioEnergyDataGridViewTextBoxColumn.DataPropertyName = "BioEnergy";
            this.bioEnergyDataGridViewTextBoxColumn.HeaderText = "BioEnergy";
            this.bioEnergyDataGridViewTextBoxColumn.Name = "bioEnergyDataGridViewTextBoxColumn";
            this.bioEnergyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastMissileHitTimeDataGridViewTextBoxColumn
            // 
            this.lastMissileHitTimeDataGridViewTextBoxColumn.DataPropertyName = "LastMissileHitTime";
            this.lastMissileHitTimeDataGridViewTextBoxColumn.HeaderText = "LastMissileHitTime";
            this.lastMissileHitTimeDataGridViewTextBoxColumn.Name = "lastMissileHitTimeDataGridViewTextBoxColumn";
            this.lastMissileHitTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastBeamHitTimeDataGridViewTextBoxColumn
            // 
            this.lastBeamHitTimeDataGridViewTextBoxColumn.DataPropertyName = "LastBeamHitTime";
            this.lastBeamHitTimeDataGridViewTextBoxColumn.HeaderText = "LastBeamHitTime";
            this.lastBeamHitTimeDataGridViewTextBoxColumn.Name = "lastBeamHitTimeDataGridViewTextBoxColumn";
            this.lastBeamHitTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastDamageTimeDataGridViewTextBoxColumn
            // 
            this.lastDamageTimeDataGridViewTextBoxColumn.DataPropertyName = "LastDamageTime";
            this.lastDamageTimeDataGridViewTextBoxColumn.HeaderText = "LastDamageTime";
            this.lastDamageTimeDataGridViewTextBoxColumn.Name = "lastDamageTimeDataGridViewTextBoxColumn";
            this.lastDamageTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastPenetratingDamageTimeDataGridViewTextBoxColumn
            // 
            this.lastPenetratingDamageTimeDataGridViewTextBoxColumn.DataPropertyName = "LastPenetratingDamageTime";
            this.lastPenetratingDamageTimeDataGridViewTextBoxColumn.HeaderText = "LastPenetratingDamageTime";
            this.lastPenetratingDamageTimeDataGridViewTextBoxColumn.Name = "lastPenetratingDamageTimeDataGridViewTextBoxColumn";
            this.lastPenetratingDamageTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // assignedFormationIDDataGridViewTextBoxColumn
            // 
            this.assignedFormationIDDataGridViewTextBoxColumn.DataPropertyName = "AssignedFormationID";
            this.assignedFormationIDDataGridViewTextBoxColumn.HeaderText = "AssignedFormationID";
            this.assignedFormationIDDataGridViewTextBoxColumn.Name = "assignedFormationIDDataGridViewTextBoxColumn";
            this.assignedFormationIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fCTShipBindingSource
            // 
            this.fCTShipBindingSource.DataSource = typeof(AuroraMarvin.FCT_Ship);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.shipName);
            this.tabPage4.Controls.Add(this.saveDesignButton);
            this.tabPage4.Controls.Add(this.shipDisplay);
            this.tabPage4.Controls.Add(this.shipDesignSelector);
            this.tabPage4.Controls.Add(this.hullSelector);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(768, 443);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Ship Designs";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // shipName
            // 
            this.shipName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.shipName.Location = new System.Drawing.Point(54, 415);
            this.shipName.Name = "shipName";
            this.shipName.Size = new System.Drawing.Size(146, 20);
            this.shipName.TabIndex = 8;
            // 
            // saveDesignButton
            // 
            this.saveDesignButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveDesignButton.Location = new System.Drawing.Point(9, 413);
            this.saveDesignButton.Name = "saveDesignButton";
            this.saveDesignButton.Size = new System.Drawing.Size(37, 23);
            this.saveDesignButton.TabIndex = 7;
            this.saveDesignButton.Text = "New";
            this.saveDesignButton.UseVisualStyleBackColor = true;
            this.saveDesignButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // shipDisplay
            // 
            this.shipDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.shipDisplay.Location = new System.Drawing.Point(206, 12);
            this.shipDisplay.Name = "shipDisplay";
            this.shipDisplay.Size = new System.Drawing.Size(556, 425);
            this.shipDisplay.TabIndex = 6;
            this.shipDisplay.Text = "";
            // 
            // shipDesignSelector
            // 
            this.shipDesignSelector.FormattingEnabled = true;
            this.shipDesignSelector.Location = new System.Drawing.Point(54, 40);
            this.shipDesignSelector.Name = "shipDesignSelector";
            this.shipDesignSelector.Size = new System.Drawing.Size(145, 21);
            this.shipDesignSelector.TabIndex = 5;
            this.shipDesignSelector.SelectedIndexChanged += new System.EventHandler(this.ShipDesignSelector_SelectedIndexChanged);
            // 
            // hullSelector
            // 
            this.hullSelector.DataSource = this.fCTHullDescriptionBindingSource;
            this.hullSelector.DisplayMember = "Description";
            this.hullSelector.FormattingEnabled = true;
            this.hullSelector.Location = new System.Drawing.Point(54, 12);
            this.hullSelector.Name = "hullSelector";
            this.hullSelector.Size = new System.Drawing.Size(146, 21);
            this.hullSelector.TabIndex = 4;
            this.hullSelector.ValueMember = "HullDescriptionID";
            this.hullSelector.SelectedIndexChanged += new System.EventHandler(this.HullSelector_SelectedIndexChanged);
            // 
            // fCTHullDescriptionBindingSource
            // 
            this.fCTHullDescriptionBindingSource.DataSource = typeof(AuroraMarvin.FCT_HullDescription);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Design";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Hull";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tabControl2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(768, 443);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Notes";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage13);
            this.tabControl2.Controls.Add(this.tabPage14);
            this.tabControl2.Location = new System.Drawing.Point(-4, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(772, 447);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.notesBox);
            this.tabPage13.Controls.Add(this.notesSaveButton);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(764, 421);
            this.tabPage13.TabIndex = 0;
            this.tabPage13.Text = "Global Notes";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // notesBox
            // 
            this.notesBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.notesBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notesBox.Location = new System.Drawing.Point(-1, 0);
            this.notesBox.Name = "notesBox";
            this.notesBox.Size = new System.Drawing.Size(765, 386);
            this.notesBox.TabIndex = 0;
            this.notesBox.Text = "";
            // 
            // notesSaveButton
            // 
            this.notesSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.notesSaveButton.Location = new System.Drawing.Point(683, 392);
            this.notesSaveButton.Name = "notesSaveButton";
            this.notesSaveButton.Size = new System.Drawing.Size(75, 23);
            this.notesSaveButton.TabIndex = 1;
            this.notesSaveButton.Text = "Update";
            this.notesSaveButton.UseVisualStyleBackColor = true;
            this.notesSaveButton.Click += new System.EventHandler(this.NotesSaveButton_Click);
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.GameNotesSaveButton);
            this.tabPage14.Controls.Add(this.gameNotesBox);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(764, 421);
            this.tabPage14.TabIndex = 1;
            this.tabPage14.Text = "Per Game and Race Notes";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // GameNotesSaveButton
            // 
            this.GameNotesSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GameNotesSaveButton.Location = new System.Drawing.Point(683, 392);
            this.GameNotesSaveButton.Name = "GameNotesSaveButton";
            this.GameNotesSaveButton.Size = new System.Drawing.Size(75, 23);
            this.GameNotesSaveButton.TabIndex = 2;
            this.GameNotesSaveButton.Text = "Update";
            this.GameNotesSaveButton.UseVisualStyleBackColor = true;
            this.GameNotesSaveButton.Click += new System.EventHandler(this.GameNotesSaveButton_Click);
            // 
            // gameNotesBox
            // 
            this.gameNotesBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gameNotesBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameNotesBox.Location = new System.Drawing.Point(0, 0);
            this.gameNotesBox.Name = "gameNotesBox";
            this.gameNotesBox.Size = new System.Drawing.Size(764, 386);
            this.gameNotesBox.TabIndex = 1;
            this.gameNotesBox.Text = "";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tabControl3);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(768, 443);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Minerals";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl3.Controls.Add(this.tabPage15);
            this.tabControl3.Controls.Add(this.tabPage16);
            this.tabControl3.Controls.Add(this.tabPage17);
            this.tabControl3.Location = new System.Drawing.Point(-3, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(775, 447);
            this.tabControl3.TabIndex = 1;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.MineralOverviewDateWarningLabel);
            this.tabPage15.Controls.Add(this.chart1);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(767, 421);
            this.tabPage15.TabIndex = 0;
            this.tabPage15.Text = "Overview";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // MineralOverviewDateWarningLabel
            // 
            this.MineralOverviewDateWarningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.MineralOverviewDateWarningLabel.AutoSize = true;
            this.MineralOverviewDateWarningLabel.Location = new System.Drawing.Point(252, 405);
            this.MineralOverviewDateWarningLabel.Name = "MineralOverviewDateWarningLabel";
            this.MineralOverviewDateWarningLabel.Size = new System.Drawing.Size(515, 13);
            this.MineralOverviewDateWarningLabel.TabIndex = 1;
            this.MineralOverviewDateWarningLabel.Text = "Dates prior to year 100 can\'t be handled here so games with starting year prior t" +
    "o 100 will be mapped to 100.";
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(-4, 0);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Maroon;
            series1.Legend = "Legend1";
            series1.Name = "Duranium";
            series1.ToolTip = "Duranium";
            series1.XValueMember = "DateTime";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series1.YValueMembers = "SUM(Duranium)";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Orange;
            series2.Legend = "Legend1";
            series2.Name = "Neutronium";
            series2.ToolTip = "Neutronium";
            series2.XValueMember = "DateTime";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series2.YValueMembers = "SUM(Neutronium)";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Navy;
            series3.Legend = "Legend1";
            series3.Name = "Corbomite";
            series3.ToolTip = "Corbomite";
            series3.XValueMember = "DateTime";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series3.YValueMembers = "SUM(Corbomite)";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.Red;
            series4.Legend = "Legend1";
            series4.Name = "Tritanium";
            series4.ToolTip = "Tritanium";
            series4.XValueMember = "DateTime";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series4.YValueMembers = "SUM(Tritanium)";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Color = System.Drawing.Color.Green;
            series5.Legend = "Legend1";
            series5.Name = "Boronide";
            series5.ToolTip = "Boronide";
            series5.XValueMember = "DateTime";
            series5.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series5.YValueMembers = "SUM(Boronide)";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Color = System.Drawing.Color.Turquoise;
            series6.Legend = "Legend1";
            series6.Name = "Mercassium";
            series6.ToolTip = "Mercassium";
            series6.XValueMember = "DateTime";
            series6.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series6.YValueMembers = "SUM(Mercassium)";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Color = System.Drawing.Color.Blue;
            series7.Legend = "Legend1";
            series7.Name = "Vendarite";
            series7.ToolTip = "Vendarite";
            series7.XValueMember = "DateTime";
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series7.YValueMembers = "SUM(Vendarite)";
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Color = System.Drawing.Color.Purple;
            series8.Legend = "Legend1";
            series8.Name = "Sorium";
            series8.ToolTip = "Sorium";
            series8.XValueMember = "DateTime";
            series8.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series8.YValueMembers = "SUM(Sorium)";
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Color = System.Drawing.Color.Magenta;
            series9.Legend = "Legend1";
            series9.Name = "Uridium";
            series9.ToolTip = "Uridium";
            series9.XValueMember = "DateTime";
            series9.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series9.YValueMembers = "SUM(Uridium)";
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Color = System.Drawing.Color.MediumSpringGreen;
            series10.Legend = "Legend1";
            series10.Name = "Corundium";
            series10.ToolTip = "Corundium";
            series10.XValueMember = "DateTime";
            series10.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series10.YValueMembers = "SUM(Corundium)";
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Color = System.Drawing.Color.Pink;
            series11.Legend = "Legend1";
            series11.Name = "Gallicite";
            series11.ToolTip = "Gallicite";
            series11.XValueMember = "DateTime";
            series11.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series11.YValueMembers = "SUM(Gallicite)";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Series.Add(series6);
            this.chart1.Series.Add(series7);
            this.chart1.Series.Add(series8);
            this.chart1.Series.Add(series9);
            this.chart1.Series.Add(series10);
            this.chart1.Series.Add(series11);
            this.chart1.Size = new System.Drawing.Size(768, 418);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.mineralChangesGridView);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(767, 421);
            this.tabPage16.TabIndex = 1;
            this.tabPage16.Text = "Changes";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // mineralChangesGridView
            // 
            this.mineralChangesGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mineralChangesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mineralChangesGridView.Location = new System.Drawing.Point(0, 0);
            this.mineralChangesGridView.Name = "mineralChangesGridView";
            this.mineralChangesGridView.Size = new System.Drawing.Size(767, 421);
            this.mineralChangesGridView.TabIndex = 0;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.MineralPopulationDateWarningLabel);
            this.tabPage17.Controls.Add(this.PopulationComboBox1);
            this.tabPage17.Controls.Add(this.chart6);
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(767, 421);
            this.tabPage17.TabIndex = 2;
            this.tabPage17.Text = "Per population";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // MineralPopulationDateWarningLabel
            // 
            this.MineralPopulationDateWarningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.MineralPopulationDateWarningLabel.AutoSize = true;
            this.MineralPopulationDateWarningLabel.Location = new System.Drawing.Point(252, 405);
            this.MineralPopulationDateWarningLabel.Name = "MineralPopulationDateWarningLabel";
            this.MineralPopulationDateWarningLabel.Size = new System.Drawing.Size(515, 13);
            this.MineralPopulationDateWarningLabel.TabIndex = 3;
            this.MineralPopulationDateWarningLabel.Text = "Dates prior to year 100 can\'t be handled here so games with starting year prior t" +
    "o 100 will be mapped to 100.";
            // 
            // PopulationComboBox1
            // 
            this.PopulationComboBox1.FormattingEnabled = true;
            this.PopulationComboBox1.Location = new System.Drawing.Point(7, 4);
            this.PopulationComboBox1.Name = "PopulationComboBox1";
            this.PopulationComboBox1.Size = new System.Drawing.Size(181, 21);
            this.PopulationComboBox1.TabIndex = 2;
            this.PopulationComboBox1.SelectedIndexChanged += new System.EventHandler(this.PopulationComboBox1_SelectedIndexChanged);
            // 
            // chart6
            // 
            this.chart6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chart6.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart6.Legends.Add(legend2);
            this.chart6.Location = new System.Drawing.Point(0, 31);
            this.chart6.Name = "chart6";
            this.chart6.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Color = System.Drawing.Color.Maroon;
            series12.Legend = "Legend1";
            series12.Name = "Duranium";
            series12.ToolTip = "Duranium";
            series12.XValueMember = "DateTime";
            series12.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series12.YValueMembers = "Duranium";
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series13.Color = System.Drawing.Color.Orange;
            series13.Legend = "Legend1";
            series13.Name = "Neutronium";
            series13.ToolTip = "Neutronium";
            series13.XValueMember = "DateTime";
            series13.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series13.YValueMembers = "Neutronium";
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Color = System.Drawing.Color.Navy;
            series14.Legend = "Legend1";
            series14.Name = "Corbomite";
            series14.ToolTip = "Corbomite";
            series14.XValueMember = "DateTime";
            series14.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series14.YValueMembers = "Corbomite";
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series15.Color = System.Drawing.Color.Red;
            series15.Legend = "Legend1";
            series15.Name = "Tritanium";
            series15.ToolTip = "Tritanium";
            series15.XValueMember = "DateTime";
            series15.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series15.YValueMembers = "Tritanium";
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series16.Color = System.Drawing.Color.Green;
            series16.Legend = "Legend1";
            series16.Name = "Boronide";
            series16.ToolTip = "Boronide";
            series16.XValueMember = "DateTime";
            series16.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series16.YValueMembers = "Boronide";
            series17.ChartArea = "ChartArea1";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series17.Color = System.Drawing.Color.Turquoise;
            series17.Legend = "Legend1";
            series17.Name = "Mercassium";
            series17.ToolTip = "Mercassium";
            series17.XValueMember = "DateTime";
            series17.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series17.YValueMembers = "Mercassium";
            series18.ChartArea = "ChartArea1";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series18.Color = System.Drawing.Color.ForestGreen;
            series18.Legend = "Legend1";
            series18.Name = "Vendarite";
            series18.ToolTip = "Vendarite";
            series18.XValueMember = "DateTime";
            series18.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series18.YValueMembers = "Vendarite";
            series19.ChartArea = "ChartArea1";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series19.Color = System.Drawing.Color.Blue;
            series19.Legend = "Legend1";
            series19.Name = "Sorium";
            series19.ToolTip = "Sorium";
            series19.XValueMember = "DateTime";
            series19.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series19.YValueMembers = "Sorium";
            series20.ChartArea = "ChartArea1";
            series20.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series20.Color = System.Drawing.Color.Purple;
            series20.Legend = "Legend1";
            series20.Name = "Uridium";
            series20.ToolTip = "Uridium";
            series20.XValueMember = "DateTime";
            series20.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series20.YValueMembers = "Uridium";
            series21.ChartArea = "ChartArea1";
            series21.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series21.Color = System.Drawing.Color.Magenta;
            series21.Legend = "Legend1";
            series21.Name = "Corundium";
            series21.ToolTip = "Corundium";
            series21.XValueMember = "DateTime";
            series21.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series21.YValueMembers = "Corundium";
            series22.ChartArea = "ChartArea1";
            series22.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series22.Color = System.Drawing.Color.Pink;
            series22.Legend = "Legend1";
            series22.Name = "Gallicite";
            series22.ToolTip = "Gallicite";
            series22.XValueMember = "DateTime";
            series22.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series22.YValueMembers = "Gallicite";
            this.chart6.Series.Add(series12);
            this.chart6.Series.Add(series13);
            this.chart6.Series.Add(series14);
            this.chart6.Series.Add(series15);
            this.chart6.Series.Add(series16);
            this.chart6.Series.Add(series17);
            this.chart6.Series.Add(series18);
            this.chart6.Series.Add(series19);
            this.chart6.Series.Add(series20);
            this.chart6.Series.Add(series21);
            this.chart6.Series.Add(series22);
            this.chart6.Size = new System.Drawing.Size(767, 388);
            this.chart6.TabIndex = 1;
            this.chart6.Text = "chart6";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.FuelDateWarningLabel);
            this.tabPage7.Controls.Add(this.fuelTankerLabel);
            this.tabPage7.Controls.Add(this.chart2);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(768, 443);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Fuel";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // FuelDateWarningLabel
            // 
            this.FuelDateWarningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.FuelDateWarningLabel.AutoSize = true;
            this.FuelDateWarningLabel.Location = new System.Drawing.Point(253, 427);
            this.FuelDateWarningLabel.Name = "FuelDateWarningLabel";
            this.FuelDateWarningLabel.Size = new System.Drawing.Size(515, 13);
            this.FuelDateWarningLabel.TabIndex = 3;
            this.FuelDateWarningLabel.Text = "Dates prior to year 100 can\'t be handled here so games with starting year prior t" +
    "o 100 will be mapped to 100.";
            // 
            // fuelTankerLabel
            // 
            this.fuelTankerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.fuelTankerLabel.AutoSize = true;
            this.fuelTankerLabel.Location = new System.Drawing.Point(3, 427);
            this.fuelTankerLabel.Name = "fuelTankerLabel";
            this.fuelTankerLabel.Size = new System.Drawing.Size(0, 13);
            this.fuelTankerLabel.TabIndex = 2;
            // 
            // chart2
            // 
            this.chart2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart2.Legends.Add(legend3);
            this.chart2.Location = new System.Drawing.Point(0, 0);
            this.chart2.Name = "chart2";
            series23.ChartArea = "ChartArea1";
            series23.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series23.Legend = "Legend1";
            series23.Name = "FuelStockpile";
            series23.XValueMember = "DateTime";
            series23.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series23.YValueMembers = "SUM(FuelStockpile)";
            this.chart2.Series.Add(series23);
            this.chart2.Size = new System.Drawing.Size(768, 443);
            this.chart2.TabIndex = 1;
            this.chart2.Text = "chart2";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.MaintenanceDateWarningLabel);
            this.tabPage8.Controls.Add(this.chart3);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(768, 443);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Maintenance Supplies";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // MaintenanceDateWarningLabel
            // 
            this.MaintenanceDateWarningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.MaintenanceDateWarningLabel.AutoSize = true;
            this.MaintenanceDateWarningLabel.Location = new System.Drawing.Point(253, 427);
            this.MaintenanceDateWarningLabel.Name = "MaintenanceDateWarningLabel";
            this.MaintenanceDateWarningLabel.Size = new System.Drawing.Size(515, 13);
            this.MaintenanceDateWarningLabel.TabIndex = 2;
            this.MaintenanceDateWarningLabel.Text = "Dates prior to year 100 can\'t be handled here so games with starting year prior t" +
    "o 100 will be mapped to 100.";
            // 
            // chart3
            // 
            this.chart3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea4.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chart3.Legends.Add(legend4);
            this.chart3.Location = new System.Drawing.Point(0, 0);
            this.chart3.Name = "chart3";
            series24.ChartArea = "ChartArea1";
            series24.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series24.Legend = "Legend1";
            series24.Name = "MaintenanceStockpile";
            series24.XValueMember = "DateTime";
            series24.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series24.YValueMembers = "SUM(MaintenanceStockpile)";
            this.chart3.Series.Add(series24);
            this.chart3.Size = new System.Drawing.Size(768, 443);
            this.chart3.TabIndex = 1;
            this.chart3.Text = "chart3";
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.PopulationDateWarningLabel);
            this.tabPage9.Controls.Add(this.chart4);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(768, 443);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Population";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // PopulationDateWarningLabel
            // 
            this.PopulationDateWarningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PopulationDateWarningLabel.AutoSize = true;
            this.PopulationDateWarningLabel.Location = new System.Drawing.Point(253, 427);
            this.PopulationDateWarningLabel.Name = "PopulationDateWarningLabel";
            this.PopulationDateWarningLabel.Size = new System.Drawing.Size(515, 13);
            this.PopulationDateWarningLabel.TabIndex = 2;
            this.PopulationDateWarningLabel.Text = "Dates prior to year 100 can\'t be handled here so games with starting year prior t" +
    "o 100 will be mapped to 100.";
            // 
            // chart4
            // 
            this.chart4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea5.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.chart4.Legends.Add(legend5);
            this.chart4.Location = new System.Drawing.Point(0, 0);
            this.chart4.Name = "chart4";
            series25.ChartArea = "ChartArea1";
            series25.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series25.Legend = "Legend1";
            series25.Name = "Population";
            series25.XValueMember = "DateTime";
            series25.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series25.YValueMembers = "SUM(Population)";
            this.chart4.Series.Add(series25);
            this.chart4.Size = new System.Drawing.Size(768, 443);
            this.chart4.TabIndex = 1;
            this.chart4.Text = "chart4";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.WealthDateWarningLabel);
            this.tabPage11.Controls.Add(this.chart5);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(768, 443);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "Wealth";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // WealthDateWarningLabel
            // 
            this.WealthDateWarningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.WealthDateWarningLabel.AutoSize = true;
            this.WealthDateWarningLabel.Location = new System.Drawing.Point(253, 427);
            this.WealthDateWarningLabel.Name = "WealthDateWarningLabel";
            this.WealthDateWarningLabel.Size = new System.Drawing.Size(515, 13);
            this.WealthDateWarningLabel.TabIndex = 3;
            this.WealthDateWarningLabel.Text = "Dates prior to year 100 can\'t be handled here so games with starting year prior t" +
    "o 100 will be mapped to 100.";
            // 
            // chart5
            // 
            this.chart5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea6.Name = "ChartArea1";
            this.chart5.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chart5.Legends.Add(legend6);
            this.chart5.Location = new System.Drawing.Point(0, 0);
            this.chart5.Name = "chart5";
            series26.ChartArea = "ChartArea1";
            series26.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series26.Legend = "Legend1";
            series26.Name = "WealthPoints";
            series26.XValueMember = "DateTime";
            series26.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series26.YValueMembers = "WealthPoints";
            this.chart5.Series.Add(series26);
            this.chart5.Size = new System.Drawing.Size(771, 443);
            this.chart5.TabIndex = 2;
            this.chart5.Text = "chart5";
            // 
            // tabPage10
            // 
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(768, 443);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "Tech Tree";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.eventColourView);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(768, 443);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "Event Colours";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // eventColourView
            // 
            this.eventColourView.AllowUserToAddRows = false;
            this.eventColourView.AllowUserToDeleteRows = false;
            this.eventColourView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventColourView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.eventColourView.Location = new System.Drawing.Point(0, 0);
            this.eventColourView.Name = "eventColourView";
            this.eventColourView.ReadOnly = true;
            this.eventColourView.Size = new System.Drawing.Size(768, 443);
            this.eventColourView.TabIndex = 0;
            // 
            // fileButton
            // 
            this.fileButton.Location = new System.Drawing.Point(12, 27);
            this.fileButton.Name = "fileButton";
            this.fileButton.Size = new System.Drawing.Size(119, 23);
            this.fileButton.TabIndex = 12;
            this.fileButton.Text = "Select Database File";
            this.fileButton.UseVisualStyleBackColor = true;
            this.fileButton.Click += new System.EventHandler(this.FileButton_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            this.fileSystemWatcher1.Changed += new System.IO.FileSystemEventHandler(this.FileSystemWatcher1_Changed);
            // 
            // GameTimeLabel
            // 
            this.GameTimeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GameTimeLabel.Location = new System.Drawing.Point(590, 32);
            this.GameTimeLabel.Name = "GameTimeLabel";
            this.GameTimeLabel.Size = new System.Drawing.Size(194, 13);
            this.GameTimeLabel.TabIndex = 13;
            this.GameTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.HelpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.ExitToolStripMenuItem.Text = "Exit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowTechTreeToolStripMenuItem,
            this.toolStripSeparator1,
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem,
            this.ShowDamagedShipsToolStripMenuItem,
            this.ShowLowCrewMoraleToolStripMenuItem,
            this.ShowFreeConstructionFactoriesToolStripMenuItem,
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem,
            this.ShowFreeFighterFactoriesToolStripMenuItem,
            this.ShowObsoleteShipsToolStripMenuItem,
            this.ShowUnusedTerraformersToolStripMenuItem,
            this.ShowUnusedMinesToolStripMenuItem,
            this.ShowShipsWithArmorDamageToolStripMenuItem,
            this.ShowWrecksToolStripMenuItem,
            this.ShowObsoleteTooledShipyardsToolStripMenuItem,
            this.ShowMissingSectorCommandersToolStripMenuItem,
            this.ShowShipWithoutMspToolStripMenuItem,
            this.ShowShipWithLowMspToolStripMenuItem,
            this.ShowMissingAdminCommandersToolStripMenuItem,
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem,
            this.ShowTaxedCivMinesToolStripMenuItem,
            this.ShowLowPopulationEfficienyToolStripMenuItem,
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem,
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem,
            this.ShowLifePodsToolStripMenuItem,
            this.ShowPopulationsWithoutGovernorToolStripMenuItem,
            this.ShowOpenFireFCToolStripMenuItem,
            this.ShowResearchFieldMismatchToolStripMenuItem,
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem,
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem,
            this.ShowIdleSoriumHarvestersToolStripMenuItem,
            this.ShowIdleOrbitalMinersToolStripMenuItem,
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem,
            this.ShowIdleGeosurveyFormationsToolStripMenuItem,
            this.ShowDormantAncientConstructsToolStripMenuItem,
            this.ShowNotActiveAncientConstructsToolStripMenuItem,
            this.ShowHostileShipContactsToolStripMenuItem,
            this.ShowHostileGroundForceContactsToolStripMenuItem,
            this.ShowIdleShipyardsToolStripMenuItem,
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // ShowTechTreeToolStripMenuItem
            // 
            this.ShowTechTreeToolStripMenuItem.Checked = true;
            this.ShowTechTreeToolStripMenuItem.CheckOnClick = true;
            this.ShowTechTreeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowTechTreeToolStripMenuItem.Name = "ShowTechTreeToolStripMenuItem";
            this.ShowTechTreeToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowTechTreeToolStripMenuItem.Text = "Show tech tree";
            this.ShowTechTreeToolStripMenuItem.Click += new System.EventHandler(this.ShowTechTreeToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(334, 6);
            // 
            // ShowAetherRiftGrowthRateReductionToolStripMenuItem
            // 
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.Checked = true;
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.CheckOnClick = true;
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.Name = "ShowAetherRiftGrowthRateReductionToolStripMenuItem";
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.Text = "Show aether rift growth rate reduction";
            this.ShowAetherRiftGrowthRateReductionToolStripMenuItem.Click += new System.EventHandler(this.ShowAetherRiftGrowthRateReductionToolStripMenuItem_Click);
            // 
            // ShowDamagedShipsToolStripMenuItem
            // 
            this.ShowDamagedShipsToolStripMenuItem.Checked = true;
            this.ShowDamagedShipsToolStripMenuItem.CheckOnClick = true;
            this.ShowDamagedShipsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowDamagedShipsToolStripMenuItem.Name = "ShowDamagedShipsToolStripMenuItem";
            this.ShowDamagedShipsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowDamagedShipsToolStripMenuItem.Text = "Show damaged ships";
            this.ShowDamagedShipsToolStripMenuItem.Click += new System.EventHandler(this.ShowDamagedShipsToolStripMenuItem_Click);
            // 
            // ShowLowCrewMoraleToolStripMenuItem
            // 
            this.ShowLowCrewMoraleToolStripMenuItem.Checked = true;
            this.ShowLowCrewMoraleToolStripMenuItem.CheckOnClick = true;
            this.ShowLowCrewMoraleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowLowCrewMoraleToolStripMenuItem.Name = "ShowLowCrewMoraleToolStripMenuItem";
            this.ShowLowCrewMoraleToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowLowCrewMoraleToolStripMenuItem.Text = "Show low crew morale";
            this.ShowLowCrewMoraleToolStripMenuItem.Click += new System.EventHandler(this.ShowLowCrewMoraleToolStripMenuItem_Click);
            // 
            // ShowFreeConstructionFactoriesToolStripMenuItem
            // 
            this.ShowFreeConstructionFactoriesToolStripMenuItem.Checked = true;
            this.ShowFreeConstructionFactoriesToolStripMenuItem.CheckOnClick = true;
            this.ShowFreeConstructionFactoriesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowFreeConstructionFactoriesToolStripMenuItem.Name = "ShowFreeConstructionFactoriesToolStripMenuItem";
            this.ShowFreeConstructionFactoriesToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowFreeConstructionFactoriesToolStripMenuItem.Text = "Show free construction factories";
            this.ShowFreeConstructionFactoriesToolStripMenuItem.Click += new System.EventHandler(this.ShowFreeConstructionFactoriesToolStripMenuItem_Click);
            // 
            // ShowFreeOrdnanceFactoriesToolStripMenuItem
            // 
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem.Checked = true;
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem.CheckOnClick = true;
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem.Name = "ShowFreeOrdnanceFactoriesToolStripMenuItem";
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem.Text = "Show free ordnance factories";
            this.ShowFreeOrdnanceFactoriesToolStripMenuItem.Click += new System.EventHandler(this.ShowFreeOrdnanceFactoriesToolStripMenuItem_Click);
            // 
            // ShowFreeFighterFactoriesToolStripMenuItem
            // 
            this.ShowFreeFighterFactoriesToolStripMenuItem.Checked = true;
            this.ShowFreeFighterFactoriesToolStripMenuItem.CheckOnClick = true;
            this.ShowFreeFighterFactoriesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowFreeFighterFactoriesToolStripMenuItem.Name = "ShowFreeFighterFactoriesToolStripMenuItem";
            this.ShowFreeFighterFactoriesToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowFreeFighterFactoriesToolStripMenuItem.Text = "Show free fighter factories";
            this.ShowFreeFighterFactoriesToolStripMenuItem.Click += new System.EventHandler(this.ShowFreeFighterFactoriesToolStripMenuItem_Click);
            // 
            // ShowObsoleteShipsToolStripMenuItem
            // 
            this.ShowObsoleteShipsToolStripMenuItem.Checked = true;
            this.ShowObsoleteShipsToolStripMenuItem.CheckOnClick = true;
            this.ShowObsoleteShipsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowObsoleteShipsToolStripMenuItem.Name = "ShowObsoleteShipsToolStripMenuItem";
            this.ShowObsoleteShipsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowObsoleteShipsToolStripMenuItem.Text = "Show obsolete ships";
            this.ShowObsoleteShipsToolStripMenuItem.Click += new System.EventHandler(this.ShowObsoleteShipsToolStripMenuItem_Click);
            // 
            // ShowUnusedTerraformersToolStripMenuItem
            // 
            this.ShowUnusedTerraformersToolStripMenuItem.Checked = true;
            this.ShowUnusedTerraformersToolStripMenuItem.CheckOnClick = true;
            this.ShowUnusedTerraformersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowUnusedTerraformersToolStripMenuItem.Name = "ShowUnusedTerraformersToolStripMenuItem";
            this.ShowUnusedTerraformersToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowUnusedTerraformersToolStripMenuItem.Text = "Show unused terraformers";
            this.ShowUnusedTerraformersToolStripMenuItem.Click += new System.EventHandler(this.ShowUnusedTerraformersToolStripMenuItem_Click);
            // 
            // ShowUnusedMinesToolStripMenuItem
            // 
            this.ShowUnusedMinesToolStripMenuItem.Checked = true;
            this.ShowUnusedMinesToolStripMenuItem.CheckOnClick = true;
            this.ShowUnusedMinesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowUnusedMinesToolStripMenuItem.Name = "ShowUnusedMinesToolStripMenuItem";
            this.ShowUnusedMinesToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowUnusedMinesToolStripMenuItem.Text = "Show unused mines";
            this.ShowUnusedMinesToolStripMenuItem.Click += new System.EventHandler(this.ShowUnusedMinesToolStripMenuItem_Click);
            // 
            // ShowShipsWithArmorDamageToolStripMenuItem
            // 
            this.ShowShipsWithArmorDamageToolStripMenuItem.Checked = true;
            this.ShowShipsWithArmorDamageToolStripMenuItem.CheckOnClick = true;
            this.ShowShipsWithArmorDamageToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowShipsWithArmorDamageToolStripMenuItem.Name = "ShowShipsWithArmorDamageToolStripMenuItem";
            this.ShowShipsWithArmorDamageToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowShipsWithArmorDamageToolStripMenuItem.Text = "Show ships with armor damage";
            this.ShowShipsWithArmorDamageToolStripMenuItem.Click += new System.EventHandler(this.ShowShipsWithArmorDamageToolStripMenuItem_Click);
            // 
            // ShowWrecksToolStripMenuItem
            // 
            this.ShowWrecksToolStripMenuItem.Checked = true;
            this.ShowWrecksToolStripMenuItem.CheckOnClick = true;
            this.ShowWrecksToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowWrecksToolStripMenuItem.Name = "ShowWrecksToolStripMenuItem";
            this.ShowWrecksToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowWrecksToolStripMenuItem.Text = "Show wrecks";
            this.ShowWrecksToolStripMenuItem.Click += new System.EventHandler(this.ShowWrecksToolStripMenuItem_Click);
            // 
            // ShowObsoleteTooledShipyardsToolStripMenuItem
            // 
            this.ShowObsoleteTooledShipyardsToolStripMenuItem.Checked = true;
            this.ShowObsoleteTooledShipyardsToolStripMenuItem.CheckOnClick = true;
            this.ShowObsoleteTooledShipyardsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowObsoleteTooledShipyardsToolStripMenuItem.Name = "ShowObsoleteTooledShipyardsToolStripMenuItem";
            this.ShowObsoleteTooledShipyardsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowObsoleteTooledShipyardsToolStripMenuItem.Text = "Show obsolete tooled shipyards";
            this.ShowObsoleteTooledShipyardsToolStripMenuItem.Click += new System.EventHandler(this.ShowObsoleteTooledShipyardsToolStripMenuItem_Click);
            // 
            // ShowMissingSectorCommandersToolStripMenuItem
            // 
            this.ShowMissingSectorCommandersToolStripMenuItem.Checked = true;
            this.ShowMissingSectorCommandersToolStripMenuItem.CheckOnClick = true;
            this.ShowMissingSectorCommandersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowMissingSectorCommandersToolStripMenuItem.Name = "ShowMissingSectorCommandersToolStripMenuItem";
            this.ShowMissingSectorCommandersToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowMissingSectorCommandersToolStripMenuItem.Text = "Show missing sector commanders";
            this.ShowMissingSectorCommandersToolStripMenuItem.Click += new System.EventHandler(this.ShowMissingSectorCommandersToolStripMenuItem_Click);
            // 
            // ShowShipWithoutMspToolStripMenuItem
            // 
            this.ShowShipWithoutMspToolStripMenuItem.Checked = true;
            this.ShowShipWithoutMspToolStripMenuItem.CheckOnClick = true;
            this.ShowShipWithoutMspToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowShipWithoutMspToolStripMenuItem.Name = "ShowShipWithoutMspToolStripMenuItem";
            this.ShowShipWithoutMspToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowShipWithoutMspToolStripMenuItem.Text = "Show ships without MSP";
            this.ShowShipWithoutMspToolStripMenuItem.Click += new System.EventHandler(this.ShowShipWithoutMspToolStripMenuItem_Click);
            // 
            // ShowShipWithLowMspToolStripMenuItem
            // 
            this.ShowShipWithLowMspToolStripMenuItem.Checked = true;
            this.ShowShipWithLowMspToolStripMenuItem.CheckOnClick = true;
            this.ShowShipWithLowMspToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowShipWithLowMspToolStripMenuItem.Name = "ShowShipWithLowMspToolStripMenuItem";
            this.ShowShipWithLowMspToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowShipWithLowMspToolStripMenuItem.Text = "Show ships with less then 50% MSPs left";
            this.ShowShipWithLowMspToolStripMenuItem.Click += new System.EventHandler(this.ShowShipWithLowMspToolStripMenuItem_Click);
            // 
            // ShowMissingAdminCommandersToolStripMenuItem
            // 
            this.ShowMissingAdminCommandersToolStripMenuItem.Checked = true;
            this.ShowMissingAdminCommandersToolStripMenuItem.CheckOnClick = true;
            this.ShowMissingAdminCommandersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowMissingAdminCommandersToolStripMenuItem.Name = "ShowMissingAdminCommandersToolStripMenuItem";
            this.ShowMissingAdminCommandersToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowMissingAdminCommandersToolStripMenuItem.Text = "Show missing admin commanders";
            this.ShowMissingAdminCommandersToolStripMenuItem.Click += new System.EventHandler(this.ShowMissingAdminCommandersToolStripMenuItem_Click);
            // 
            // ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem
            // 
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.Checked = true;
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.CheckOnClick = true;
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.Name = "ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem";
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.Text = "Show CMC mines without mass driver destination";
            this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem.Click += new System.EventHandler(this.ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem_Click);
            // 
            // ShowTaxedCivMinesToolStripMenuItem
            // 
            this.ShowTaxedCivMinesToolStripMenuItem.Checked = true;
            this.ShowTaxedCivMinesToolStripMenuItem.CheckOnClick = true;
            this.ShowTaxedCivMinesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowTaxedCivMinesToolStripMenuItem.Name = "ShowTaxedCivMinesToolStripMenuItem";
            this.ShowTaxedCivMinesToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowTaxedCivMinesToolStripMenuItem.Text = "Show taxed CMC mines";
            this.ShowTaxedCivMinesToolStripMenuItem.Click += new System.EventHandler(this.ShowTaxedCivMinesToolStripMenuItem_Click);
            // 
            // ShowLowPopulationEfficienyToolStripMenuItem
            // 
            this.ShowLowPopulationEfficienyToolStripMenuItem.Checked = true;
            this.ShowLowPopulationEfficienyToolStripMenuItem.CheckOnClick = true;
            this.ShowLowPopulationEfficienyToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowLowPopulationEfficienyToolStripMenuItem.Name = "ShowLowPopulationEfficienyToolStripMenuItem";
            this.ShowLowPopulationEfficienyToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowLowPopulationEfficienyToolStripMenuItem.Text = "Show low population manufacturing efficieny";
            this.ShowLowPopulationEfficienyToolStripMenuItem.Click += new System.EventHandler(this.ShowLowPopulationEfficienyToolStripMenuItem_Click);
            // 
            // ShowSelfSustainingColonistDestinationToolStripMenuItem
            // 
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem.Checked = true;
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem.CheckOnClick = true;
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem.Name = "ShowSelfSustainingColonistDestinationToolStripMenuItem";
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem.Text = "Show self sustaining colonist destination";
            this.ShowSelfSustainingColonistDestinationToolStripMenuItem.Click += new System.EventHandler(this.ShowSelfSustainingColonistDestinationToolStripMenuItem_Click);
            // 
            // ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem
            // 
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.Checked = true;
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.CheckOnClick = true;
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.Name = "ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem";
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.Text = "Show fully trained ships in training fleets";
            this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem.Click += new System.EventHandler(this.ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem_Click);
            // 
            // ShowLifePodsToolStripMenuItem
            // 
            this.ShowLifePodsToolStripMenuItem.Checked = true;
            this.ShowLifePodsToolStripMenuItem.CheckOnClick = true;
            this.ShowLifePodsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowLifePodsToolStripMenuItem.Name = "ShowLifePodsToolStripMenuItem";
            this.ShowLifePodsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowLifePodsToolStripMenuItem.Text = "Show life pods";
            this.ShowLifePodsToolStripMenuItem.Click += new System.EventHandler(this.ShowLifePodsToolStripMenuItem_Click);
            // 
            // ShowPopulationsWithoutGovernorToolStripMenuItem
            // 
            this.ShowPopulationsWithoutGovernorToolStripMenuItem.Checked = true;
            this.ShowPopulationsWithoutGovernorToolStripMenuItem.CheckOnClick = true;
            this.ShowPopulationsWithoutGovernorToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowPopulationsWithoutGovernorToolStripMenuItem.Name = "ShowPopulationsWithoutGovernorToolStripMenuItem";
            this.ShowPopulationsWithoutGovernorToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowPopulationsWithoutGovernorToolStripMenuItem.Text = "Show populations without governor";
            this.ShowPopulationsWithoutGovernorToolStripMenuItem.Click += new System.EventHandler(this.ShowPopulationsWithoutGovernorToolStripMenuItem_Click);
            // 
            // ShowOpenFireFCToolStripMenuItem
            // 
            this.ShowOpenFireFCToolStripMenuItem.Checked = true;
            this.ShowOpenFireFCToolStripMenuItem.CheckOnClick = true;
            this.ShowOpenFireFCToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowOpenFireFCToolStripMenuItem.Name = "ShowOpenFireFCToolStripMenuItem";
            this.ShowOpenFireFCToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowOpenFireFCToolStripMenuItem.Text = "Show open fire FC";
            this.ShowOpenFireFCToolStripMenuItem.Click += new System.EventHandler(this.ShowOpenFireFCToolStripMenuItem_Click);
            // 
            // ShowResearchFieldMismatchToolStripMenuItem
            // 
            this.ShowResearchFieldMismatchToolStripMenuItem.Checked = true;
            this.ShowResearchFieldMismatchToolStripMenuItem.CheckOnClick = true;
            this.ShowResearchFieldMismatchToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowResearchFieldMismatchToolStripMenuItem.Name = "ShowResearchFieldMismatchToolStripMenuItem";
            this.ShowResearchFieldMismatchToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowResearchFieldMismatchToolStripMenuItem.Text = "Show research field mismatch";
            this.ShowResearchFieldMismatchToolStripMenuItem.Click += new System.EventHandler(this.ShowResearchFieldMismatchToolStripMenuItem_Click);
            // 
            // ShowResearchWithoutResearchFacilityToolStripMenuItem
            // 
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem.Checked = true;
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem.CheckOnClick = true;
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem.Name = "ShowResearchWithoutResearchFacilityToolStripMenuItem";
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem.Text = "Show research without research facility";
            this.ShowResearchWithoutResearchFacilityToolStripMenuItem.Click += new System.EventHandler(this.ShowResearchWithoutResearchFacilityToolStripMenuItem_Click);
            // 
            // ShowSystemsWithUnsurveyedBodiesToolStripMenuItem
            // 
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.Checked = true;
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.CheckOnClick = true;
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.Name = "ShowSystemsWithUnsurveyedBodiesToolStripMenuItem";
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.Text = "Show systems with unsurveyed bodies";
            this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem.Click += new System.EventHandler(this.ShowSystemsWithUnsurveyedBodiesToolStripMenuItem_Click);
            // 
            // ShowIdleSoriumHarvestersToolStripMenuItem
            // 
            this.ShowIdleSoriumHarvestersToolStripMenuItem.Checked = true;
            this.ShowIdleSoriumHarvestersToolStripMenuItem.CheckOnClick = true;
            this.ShowIdleSoriumHarvestersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowIdleSoriumHarvestersToolStripMenuItem.Name = "ShowIdleSoriumHarvestersToolStripMenuItem";
            this.ShowIdleSoriumHarvestersToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowIdleSoriumHarvestersToolStripMenuItem.Text = "Show idle Sorium harvesters";
            this.ShowIdleSoriumHarvestersToolStripMenuItem.Click += new System.EventHandler(this.ShowIdleSoriumHarvestersToolStripMenuItem_Click);
            // 
            // ShowIdleOrbitalMinersToolStripMenuItem
            // 
            this.ShowIdleOrbitalMinersToolStripMenuItem.Checked = true;
            this.ShowIdleOrbitalMinersToolStripMenuItem.CheckOnClick = true;
            this.ShowIdleOrbitalMinersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowIdleOrbitalMinersToolStripMenuItem.Name = "ShowIdleOrbitalMinersToolStripMenuItem";
            this.ShowIdleOrbitalMinersToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowIdleOrbitalMinersToolStripMenuItem.Text = "Show idle orbital miners";
            this.ShowIdleOrbitalMinersToolStripMenuItem.Click += new System.EventHandler(this.ShowIdleOrbitalMinersToolStripMenuItem_Click);
            // 
            // ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem
            // 
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.Checked = true;
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.CheckOnClick = true;
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.Name = "ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem";
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.Text = "Show populations with ground survey potential";
            this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem.Click += new System.EventHandler(this.ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem_Click);
            // 
            // ShowIdleGeosurveyFormationsToolStripMenuItem
            // 
            this.ShowIdleGeosurveyFormationsToolStripMenuItem.Checked = true;
            this.ShowIdleGeosurveyFormationsToolStripMenuItem.CheckOnClick = true;
            this.ShowIdleGeosurveyFormationsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowIdleGeosurveyFormationsToolStripMenuItem.Name = "ShowIdleGeosurveyFormationsToolStripMenuItem";
            this.ShowIdleGeosurveyFormationsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowIdleGeosurveyFormationsToolStripMenuItem.Text = "Show idle geosurvey formations";
            this.ShowIdleGeosurveyFormationsToolStripMenuItem.Click += new System.EventHandler(this.ShowIdleGeosurveyFormationsToolStripMenuItem_Click);
            // 
            // ShowDormantAncientConstructsToolStripMenuItem
            // 
            this.ShowDormantAncientConstructsToolStripMenuItem.Checked = true;
            this.ShowDormantAncientConstructsToolStripMenuItem.CheckOnClick = true;
            this.ShowDormantAncientConstructsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowDormantAncientConstructsToolStripMenuItem.Name = "ShowDormantAncientConstructsToolStripMenuItem";
            this.ShowDormantAncientConstructsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowDormantAncientConstructsToolStripMenuItem.Text = "Show dormant ancient constructs";
            this.ShowDormantAncientConstructsToolStripMenuItem.Click += new System.EventHandler(this.ShowDormantAncientConstructsToolStripMenuItem_Click);
            // 
            // ShowNotActiveAncientConstructsToolStripMenuItem
            // 
            this.ShowNotActiveAncientConstructsToolStripMenuItem.Checked = true;
            this.ShowNotActiveAncientConstructsToolStripMenuItem.CheckOnClick = true;
            this.ShowNotActiveAncientConstructsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowNotActiveAncientConstructsToolStripMenuItem.Name = "ShowNotActiveAncientConstructsToolStripMenuItem";
            this.ShowNotActiveAncientConstructsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowNotActiveAncientConstructsToolStripMenuItem.Text = "Show not active ancient constructs";
            this.ShowNotActiveAncientConstructsToolStripMenuItem.Click += new System.EventHandler(this.ShowNotActiveAncientConstructsToolStripMenuItem_Click);
            // 
            // ShowHostileShipContactsToolStripMenuItem
            // 
            this.ShowHostileShipContactsToolStripMenuItem.Checked = true;
            this.ShowHostileShipContactsToolStripMenuItem.CheckOnClick = true;
            this.ShowHostileShipContactsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowHostileShipContactsToolStripMenuItem.Name = "ShowHostileShipContactsToolStripMenuItem";
            this.ShowHostileShipContactsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowHostileShipContactsToolStripMenuItem.Text = "Show hostile ship contacts";
            this.ShowHostileShipContactsToolStripMenuItem.Click += new System.EventHandler(this.ShowHostileShipContactsToolStripMenuItem_Click);
            // 
            // ShowHostileGroundForceContactsToolStripMenuItem
            // 
            this.ShowHostileGroundForceContactsToolStripMenuItem.Checked = true;
            this.ShowHostileGroundForceContactsToolStripMenuItem.CheckOnClick = true;
            this.ShowHostileGroundForceContactsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowHostileGroundForceContactsToolStripMenuItem.Name = "ShowHostileGroundForceContactsToolStripMenuItem";
            this.ShowHostileGroundForceContactsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowHostileGroundForceContactsToolStripMenuItem.Text = "Show hostile ground force contacts";
            this.ShowHostileGroundForceContactsToolStripMenuItem.Click += new System.EventHandler(this.ShowHostileGroundForceContactsToolStripMenuItem_Click);
            // 
            // ShowIdleShipyardsToolStripMenuItem
            // 
            this.ShowIdleShipyardsToolStripMenuItem.Checked = true;
            this.ShowIdleShipyardsToolStripMenuItem.CheckOnClick = true;
            this.ShowIdleShipyardsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowIdleShipyardsToolStripMenuItem.Name = "ShowIdleShipyardsToolStripMenuItem";
            this.ShowIdleShipyardsToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowIdleShipyardsToolStripMenuItem.Text = "Show idle shipyards";
            this.ShowIdleShipyardsToolStripMenuItem.Click += new System.EventHandler(this.ShowIdleShipyardsToolStripMenuItem_Click);
            // 
            // ShowIdleGroundForceConstructionComplexToolStripMenuItem
            // 
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.Checked = true;
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.CheckOnClick = true;
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.Name = "ShowIdleGroundForceConstructionComplexToolStripMenuItem";
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.Size = new System.Drawing.Size(337, 22);
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.Text = "Show idle ground force construction complex";
            this.ShowIdleGroundForceConstructionComplexToolStripMenuItem.Click += new System.EventHandler(this.ShowIdleGroundForceConstructionComplexToolStripMenuItem_Click);
            // 
            // HelpToolStripMenuItem
            // 
            this.HelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AboutToolStripMenuItem});
            this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
            this.HelpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.HelpToolStripMenuItem.Text = "Help";
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.AboutToolStripMenuItem.Text = "About";
            this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // AuroraMarvinView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 537);
            this.Controls.Add(this.GameTimeLabel);
            this.Controls.Add(this.fileButton);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.raceSelector);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gameSelector);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AuroraMarvinView";
            this.Text = "Aurora Marvin";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.populationViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fCTPopulationBindingSource)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.shipViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fCTShipBindingSource)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fCTHullDescriptionBindingSource)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage13.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mineralChangesGridView)).EndInit();
            this.tabPage17.ResumeLayout(false);
            this.tabPage17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.eventColourView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox gameSelector;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox raceSelector;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button fileButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView populationViewer;
        private System.Windows.Forms.BindingSource fCTPopulationBindingSource;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView shipViewer;
        private System.Windows.Forms.BindingSource fCTShipBindingSource;
        private System.Windows.Forms.ListBox overviewList;
        private System.Windows.Forms.DataGridViewTextBoxColumn popNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn academyOfficersDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn capitalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn terraformStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchaseCivilianMineralsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colonistDestinationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn efficiencyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fighterDestFleetIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fuelProdStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fuelStockpileDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn genModSpeciesIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn groundAttackIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn groundAttackTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastColonyCostDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maintenanceStockpileDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maintProdStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn massDriverDestDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxAtmDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn noStatusChangeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn politicalStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn populationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn previousUnrestDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn provideColonistsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reqInfDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusPointsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn systemIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn systemBodyIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tempMFDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn terraformingGasIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn unrestPointsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn duraniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn neutroniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn corbomiteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tritaniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn boronideDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mercassiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vendariteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn soriumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uridiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn corundiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn galliciteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastDuraniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNeutroniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastCorbomiteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastTritaniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastBoronideDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastMercassiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastVendariteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastSoriumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastUridiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastCorundiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastGalliciteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveDuraniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveNeutroniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveCorbomiteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveTritaniumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveBoronideDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveMercassiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveVendariteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveSoriumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveUridiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveCorundiumDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reserveGalliciteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn groundGeoSurveyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn destroyedInstallationSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aIValueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn invasionStagingPointDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn originalRaceIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn doNotDeleteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn militaryRestrictedColonyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn academyNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn spaceStationDestFleetIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn importanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn autoAssignDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bonusOneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bonusTwoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bonusThreeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fleetIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subFleetIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn activeSensorsOnDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedMSIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn autofireDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn boardingCombatClockDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn constructedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn crewMoraleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentCrewDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentShieldStrengthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn damageControlIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn destroyedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fireDelayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fuelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gradePointsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn holdTechDataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn killTonnageCommercialDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn killTonnageMilitaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastLaunchTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastOverhaulDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastShoreLeaveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn launchMoraleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maintenanceStateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mothershipIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn raceIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refuelPriorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn refuelStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scrapFlagDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sensorDelayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shieldsActiveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipClassIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipFuelEfficiencyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipNotesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shippingLineIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn speciesIDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn squadronIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn syncFireDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tFPointsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transponderActiveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ordnanceTransferStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hangarLoadTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn resupplyPriorityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentMaintSuppliesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn automatedDamageControlDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tractorTargetShipIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tractorTargetShipyardIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tractorParentShipIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn overhaulFactorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bioEnergyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastMissileHitTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastBeamHitTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastDamageTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastPenetratingDamageTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedFormationIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox shipDesignSelector;
        private System.Windows.Forms.ComboBox hullSelector;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox shipDisplay;
        private System.Windows.Forms.BindingSource fCTHullDescriptionBindingSource;
        private System.Windows.Forms.Button saveDesignButton;
        private System.Windows.Forms.TextBox shipName;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button notesSaveButton;
        private System.Windows.Forms.RichTextBox notesBox;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Label GameTimeLabel;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowTechTreeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView eventColourView;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ShowAetherRiftGrowthRateReductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowDamagedShipsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowLowCrewMoraleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowFreeConstructionFactoriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowFreeOrdnanceFactoriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowFreeFighterFactoriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowObsoleteShipsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowUnusedTerraformersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowUnusedMinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowShipsWithArmorDamageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowWrecksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowObsoleteTooledShipyardsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowMissingSectorCommandersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowShipWithoutMspToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowShipWithLowMspToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowMissingAdminCommandersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowCivMinesWithoutMassDriverDestinationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowTaxedCivMinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowLowPopulationEfficienyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowSelfSustainingColonistDestinationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowFullTrainedShipsinTrainingFleetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowLifePodsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowPopulationsWithoutGovernorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowOpenFireFCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowResearchFieldMismatchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowResearchWithoutResearchFacilityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowSystemsWithUnsurveyedBodiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowIdleSoriumHarvestersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowIdleOrbitalMinersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowPopulationsWithGroundSurveyPotentialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowIdleGeosurveyFormationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowDormantAncientConstructsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowNotActiveAncientConstructsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowHostileShipContactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowHostileGroundForceContactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowIdleShipyardsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShowIdleGroundForceConstructionComplexToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.RichTextBox gameNotesBox;
        private System.Windows.Forms.Button GameNotesSaveButton;
        private System.Windows.Forms.Label fuelTankerLabel;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.DataGridView mineralChangesGridView;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.ComboBox PopulationComboBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart6;
        private System.Windows.Forms.Label MineralOverviewDateWarningLabel;
        private System.Windows.Forms.Label MineralPopulationDateWarningLabel;
        private System.Windows.Forms.Label FuelDateWarningLabel;
        private System.Windows.Forms.Label MaintenanceDateWarningLabel;
        private System.Windows.Forms.Label PopulationDateWarningLabel;
        private System.Windows.Forms.Label WealthDateWarningLabel;
    }
}

